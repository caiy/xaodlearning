pathditaumet="../../run/"

#mkdir build
#cd build && rm -rf *
#cmake ../
cd build
make -j 8

name=( "2Dtest 2Dtest 2Dtest 2" "R_Truth_taupt R_LCTopo_taupt R_EMPF_taupt 1" "Truth_taupt Matched_LCTopo_taupt Matched_EMPF_taupt 1" "Truth_taueta Matched_LCTopo_taueta Matched_EMPF_taueta 1" "Truth_taudecay Matched_LCTopo_taudecay Matched_EMPF_taudecay 1")

#cd ../ditaumet
cd ../ditaumet && rm -rf *
cp ../build/Ploter .

for ((i = 0; i < ${#name[@]}; i++))
do
	names=(${name[$i]})
	./Ploter "${names[0]}" "${names[1]}" "${names[2]}" "${names[3]}"
done
cd ..
