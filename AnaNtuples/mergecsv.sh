
csvfile=" cutflow_Higgs.csv cutflow_MultiBoson.csv cutflow_Top.csv cutflow_W.csv cutflow_Z.csv cutflow_StauStauVBF_90_40.csv cutflow_StauStauVBF_100_60.csv cutflow_StauStauVBF_90_60.csv cutflow_StauStauVBF_100_90.csv "
output="output.csv"
Transpose="Toutput.csv"

cd run
awk '(NR == 1) || (FNR > 1)' ${csvfile} > ${output}
#cat cutflow_Higgs.csv cutflow_MultiBoson.csv cutflow_Top.csv cutflow_W.csv cutflow_Z.csv cutflow_StauStauVBF_90_40.csv cutflow_StauStauVBF_100_60.csv cutflow_StauStauVBF_90_60.csv cutflow_StauStauVBF_100_90.csv > output.csv
mv ${output} ../
cd ..

awk '
BEGIN{FS=OFS=","}
{ 
    for (i=1; i<=NF; i++)  {
	        a[NR,i] = $i
		}
}
NF>p { p = NF }
END {    
	for(j=1; j<=p; j++) {
		str=a[1,j]
		for(i=2; i<=NR; i++){
			str=str","a[i,j];
		}
		print str
	}
}' ${output} > ${Transpose}
#awk '{ for (i=1; i<=NF; i++) RtoC[i]= (RtoC[i]? RtoC[i] FS $i: $i) } 
#    END{ for (i in RtoC) print RtoC[i] }' ${output} > bigfile.csv
