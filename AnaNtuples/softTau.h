#pragma once

#include <TROOT.h>
#include <vector>
#include <TChain.h>
#include <TFile.h>
#include "AnaTree.h"
#include "AnaObjs.h"

#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class softTau : public AnaTree {
public :
   void Init(TChain* tree) override;
 	softTau(TChain* chain) :AnaTree(chain) {
 		Init(this->fChain);
 	}
	
//	AnaObjs getMatedEMPFJets(){
//		AnaObjs jetVec;
//		for(unsigned int i = 0; i < this->jets_pt->size(); i++){
//			int jetID(0);
//			jetID |= JetIsSignal;
//			
//			AnaObj jet(JET, 0, jetID);
//			jet.SetPtEtaPhiM(this->jets_pt->at(i)/1000,
//						this->jets_eta->at(i),
//						this->jets_phi->at(i),
//						this->jets_m->at(i)/1000);
//			jetVec.emplace_back(jet);
//		}
//		return jetVec;
//	}
	

   // Declaration of leaf types
   UInt_t          RunNumber;
   ULong64_t       EventNumber;
   Int_t           nMatchedEMPFJetNumber;
   Int_t           nMatchedLCTopoJetNumber;
   vector<double>   *EMPFjets_eta;
   vector<double>   *EMPFjets_phi;
   vector<double>   *EMPFjets_pt;
   vector<double>   *EMPFjets_e;
   vector<double>   *EMPFjets_m;
   vector<double>   *EMPFjets_r;
   vector<double>   *LCTopojets_eta;
   vector<double>   *LCTopojets_phi;
   vector<double>   *LCTopojets_pt;
   vector<double>   *LCTopojets_e;
   vector<double>   *LCTopojets_m;
   vector<double>   *LCTopojets_r;
   vector<double>   *taus_eta;
   vector<double>   *taus_phi;
   vector<double>   *taus_pt;
   vector<double>   *taus_e;
   vector<double>   *taus_e_np;
   vector<double>   *taus_m;
   vector<unsigned long> *taus_nChargedPion;
   vector<unsigned long> *taus_nNeutralPion;
   vector<double>   *taujets_eta;
   vector<double>   *taujets_phi;
   vector<double>   *taujets_pt;
   vector<double>   *taujets_e;
   vector<double>   *taujets_m;
   vector<double>   *taujets_r;
   vector<double>   *MatchedEMPFjets_eta;
   vector<double>   *MatchedEMPFjets_phi;
   vector<double>   *MatchedEMPFjets_pt;
   vector<double>   *MatchedEMPFjets_e;
   vector<double>   *MatchedEMPFjets_m;
   vector<double>   *MatchedEMPFjets_r;
   vector<unsigned long> *MatchedEMPFjets_nChargedTrack;
   vector<unsigned long> *MatchedEMPFjets_nChargedPFO;
   vector<unsigned long> *MatchedEMPFjets_nNeutralPFO;
   vector<double>   *MatchedLCTopojets_eta;
   vector<double>   *MatchedLCTopojets_phi;
   vector<double>   *MatchedLCTopojets_pt;
   vector<double>   *MatchedLCTopojets_e;
   vector<double>   *MatchedLCTopojets_m;
   vector<double>   *MatchedLCTopojets_r;
   vector<unsigned long> *MatchedLCTopojets_nChargedTrack;
   vector<unsigned long> *MatchedLCTopojets_nChargedPFO;
   vector<unsigned long> *MatchedLCTopojets_nNeutralPFO;
   vector<double>   *MatchedEMPFtaus_eta;
   vector<double>   *MatchedEMPFtaus_phi;
   vector<double>   *MatchedEMPFtaus_pt;
   vector<double>   *MatchedEMPFtaus_e;
   vector<double>   *MatchedEMPFtaus_e_np;
   vector<double>   *MatchedEMPFtaus_m;
   vector<unsigned long> *MatchedEMPFtaus_nChargedPion;
   vector<unsigned long> *MatchedEMPFtaus_nNeutralPion;
   vector<double>   *MatchedLCTopotaus_eta;
   vector<double>   *MatchedLCTopotaus_phi;
   vector<double>   *MatchedLCTopotaus_pt;
   vector<double>   *MatchedLCTopotaus_e;
   vector<double>   *MatchedLCTopotaus_e_np;
   vector<double>   *MatchedLCTopotaus_m;
   vector<unsigned long> *MatchedLCTopotaus_nChargedPion;
   vector<unsigned long> *MatchedLCTopotaus_nNeutralPion;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_nMatchedEMPFJetNumber;   //!
   TBranch        *b_nMatchedLCTopoJetNumber;   //!
   TBranch        *b_EMPFjets_eta;   //!
   TBranch        *b_EMPFjets_phi;   //!
   TBranch        *b_EMPFjets_pt;   //!
   TBranch        *b_EMPFjets_e;   //!
   TBranch        *b_EMPFjets_m;   //!
   TBranch        *b_EMPFjets_r;   //!
   TBranch        *b_LCTopojets_eta;   //!
   TBranch        *b_LCTopojets_phi;   //!
   TBranch        *b_LCTopojets_pt;   //!
   TBranch        *b_LCTopojets_e;   //!
   TBranch        *b_LCTopojets_m;   //!
   TBranch        *b_LCTopojets_r;   //!
   TBranch        *b_taus_eta;   //!
   TBranch        *b_taus_phi;   //!
   TBranch        *b_taus_pt;   //!
   TBranch        *b_taus_e;   //!
   TBranch        *b_taus_e_np;   //!
   TBranch        *b_taus_m;   //!
   TBranch        *b_taus_nChargedPion;   //!
   TBranch        *b_taus_nNeutralPion;   //!
   TBranch        *b_taujets_eta;   //!
   TBranch        *b_taujets_phi;   //!
   TBranch        *b_taujets_pt;   //!
   TBranch        *b_taujets_e;   //!
   TBranch        *b_taujets_m;   //!
   TBranch        *b_taujets_r;   //!
   TBranch        *b_MatchedEMPFjets_eta;   //!
   TBranch        *b_MatchedEMPFjets_phi;   //!
   TBranch        *b_MatchedEMPFjets_pt;   //!
   TBranch        *b_MatchedEMPFjets_e;   //!
   TBranch        *b_MatchedEMPFjets_m;   //!
   TBranch        *b_MatchedEMPFjets_r;   //!
   TBranch        *b_MatchedEMPFjets_nChargedTrack;   //!
   TBranch        *b_MatchedEMPFjets_nChargedPFO;   //!
   TBranch        *b_MatchedEMPFjets_nNeutralPFO;   //!
   TBranch        *b_MatchedLCTopojets_eta;   //!
   TBranch        *b_MatchedLCTopojets_phi;   //!
   TBranch        *b_MatchedLCTopojets_pt;   //!
   TBranch        *b_MatchedLCTopojets_e;   //!
   TBranch        *b_MatchedLCTopojets_m;   //!
   TBranch        *b_MatchedLCTopojets_r;   //!
   TBranch        *b_MatchedLCTopojets_nChargedTrack;   //!
   TBranch        *b_MatchedLCTopojets_nChargedPFO;   //!
   TBranch        *b_MatchedLCTopojets_nNeutralPFO;   //!
   TBranch        *b_MatchedEMPFtaus_eta;   //!
   TBranch        *b_MatchedEMPFtaus_phi;   //!
   TBranch        *b_MatchedEMPFtaus_pt;   //!
   TBranch        *b_MatchedEMPFtaus_e;   //!
   TBranch        *b_MatchedEMPFtaus_e_np;   //!
   TBranch        *b_MatchedEMPFtaus_m;   //!
   TBranch        *b_MatchedEMPFtaus_nChargedPion;   //!
   TBranch        *b_MatchedEMPFtaus_nNeutralPion;   //!
   TBranch        *b_MatchedLCTopotaus_eta;   //!
   TBranch        *b_MatchedLCTopotaus_phi;   //!
   TBranch        *b_MatchedLCTopotaus_pt;   //!
   TBranch        *b_MatchedLCTopotaus_e;   //!
   TBranch        *b_MatchedLCTopotaus_e_np;   //!
   TBranch        *b_MatchedLCTopotaus_m;   //!
   TBranch        *b_MatchedLCTopotaus_nChargedPion;   //!
   TBranch        *b_MatchedLCTopotaus_nNeutralPion;   //!

};

void softTau::Init(TChain *tree)
{

   // Set object pointer
   EMPFjets_eta = 0;
   EMPFjets_phi = 0;
   EMPFjets_pt = 0;
   EMPFjets_e = 0;
   EMPFjets_m = 0;
   EMPFjets_r = 0;
   LCTopojets_eta = 0;
   LCTopojets_phi = 0;
   LCTopojets_pt = 0;
   LCTopojets_e = 0;
   LCTopojets_m = 0;
   LCTopojets_r = 0;
   taus_eta = 0;
   taus_phi = 0;
   taus_pt = 0;
   taus_e = 0;
   taus_e_np = 0;
   taus_m = 0;
   taus_nChargedPion = 0;
   taus_nNeutralPion = 0;
   taujets_eta = 0;
   taujets_phi = 0;
   taujets_pt = 0;
   taujets_e = 0;
   taujets_m = 0;
   taujets_r = 0;
   MatchedEMPFjets_eta = 0;
   MatchedEMPFjets_phi = 0;
   MatchedEMPFjets_pt = 0;
   MatchedEMPFjets_e = 0;
   MatchedEMPFjets_m = 0;
   MatchedEMPFjets_r = 0;
   MatchedEMPFjets_nChargedTrack = 0;
   MatchedEMPFjets_nChargedPFO = 0;
   MatchedEMPFjets_nNeutralPFO = 0;
   MatchedLCTopojets_eta = 0;
   MatchedLCTopojets_phi = 0;
   MatchedLCTopojets_pt = 0;
   MatchedLCTopojets_e = 0;
   MatchedLCTopojets_m = 0;
   MatchedLCTopojets_r = 0;
   MatchedLCTopojets_nChargedTrack = 0;
   MatchedLCTopojets_nChargedPFO = 0;
   MatchedLCTopojets_nNeutralPFO = 0;
   MatchedEMPFtaus_eta = 0;
   MatchedEMPFtaus_phi = 0;
   MatchedEMPFtaus_pt = 0;
   MatchedEMPFtaus_e = 0;
   MatchedEMPFtaus_e_np = 0;
   MatchedEMPFtaus_m = 0;
   MatchedEMPFtaus_nChargedPion = 0;
   MatchedEMPFtaus_nNeutralPion = 0;
   MatchedLCTopotaus_eta = 0;
   MatchedLCTopotaus_phi = 0;
   MatchedLCTopotaus_pt = 0;
   MatchedLCTopotaus_e = 0;
   MatchedLCTopotaus_e_np = 0;
   MatchedLCTopotaus_m = 0;
   MatchedLCTopotaus_nChargedPion = 0;
   MatchedLCTopotaus_nNeutralPion = 0;

	fChain = tree;
	
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("nMatchedEMPFJetNumber", &nMatchedEMPFJetNumber, &b_nMatchedEMPFJetNumber);
   fChain->SetBranchAddress("nMatchedLCTopoJetNumber", &nMatchedLCTopoJetNumber, &b_nMatchedLCTopoJetNumber);
   fChain->SetBranchAddress("EMPFjets_eta", &EMPFjets_eta, &b_EMPFjets_eta);
   fChain->SetBranchAddress("EMPFjets_phi", &EMPFjets_phi, &b_EMPFjets_phi);
   fChain->SetBranchAddress("EMPFjets_pt", &EMPFjets_pt, &b_EMPFjets_pt);
   fChain->SetBranchAddress("EMPFjets_e", &EMPFjets_e, &b_EMPFjets_e);
   fChain->SetBranchAddress("EMPFjets_m", &EMPFjets_m, &b_EMPFjets_m);
   fChain->SetBranchAddress("EMPFjets_r", &EMPFjets_r, &b_EMPFjets_r);
   fChain->SetBranchAddress("LCTopojets_eta", &LCTopojets_eta, &b_LCTopojets_eta);
   fChain->SetBranchAddress("LCTopojets_phi", &LCTopojets_phi, &b_LCTopojets_phi);
   fChain->SetBranchAddress("LCTopojets_pt", &LCTopojets_pt, &b_LCTopojets_pt);
   fChain->SetBranchAddress("LCTopojets_e", &LCTopojets_e, &b_LCTopojets_e);
   fChain->SetBranchAddress("LCTopojets_m", &LCTopojets_m, &b_LCTopojets_m);
   fChain->SetBranchAddress("LCTopojets_r", &LCTopojets_r, &b_LCTopojets_r);
   fChain->SetBranchAddress("taus_eta", &taus_eta, &b_taus_eta);
   fChain->SetBranchAddress("taus_phi", &taus_phi, &b_taus_phi);
   fChain->SetBranchAddress("taus_pt", &taus_pt, &b_taus_pt);
   fChain->SetBranchAddress("taus_e", &taus_e, &b_taus_e);
   fChain->SetBranchAddress("taus_e_np", &taus_e_np, &b_taus_e_np);
   fChain->SetBranchAddress("taus_m", &taus_m, &b_taus_m);
   fChain->SetBranchAddress("taus_nChargedPion", &taus_nChargedPion, &b_taus_nChargedPion);
   fChain->SetBranchAddress("taus_nNeutralPion", &taus_nNeutralPion, &b_taus_nNeutralPion);
   fChain->SetBranchAddress("taujets_eta", &taujets_eta, &b_taujets_eta);
   fChain->SetBranchAddress("taujets_phi", &taujets_phi, &b_taujets_phi);
   fChain->SetBranchAddress("taujets_pt", &taujets_pt, &b_taujets_pt);
   fChain->SetBranchAddress("taujets_e", &taujets_e, &b_taujets_e);
   fChain->SetBranchAddress("taujets_m", &taujets_m, &b_taujets_m);
   fChain->SetBranchAddress("taujets_r", &taujets_r, &b_taujets_r);
   fChain->SetBranchAddress("MatchedEMPFjets_eta", &MatchedEMPFjets_eta, &b_MatchedEMPFjets_eta);
   fChain->SetBranchAddress("MatchedEMPFjets_phi", &MatchedEMPFjets_phi, &b_MatchedEMPFjets_phi);
   fChain->SetBranchAddress("MatchedEMPFjets_pt", &MatchedEMPFjets_pt, &b_MatchedEMPFjets_pt);
   fChain->SetBranchAddress("MatchedEMPFjets_e", &MatchedEMPFjets_e, &b_MatchedEMPFjets_e);
   fChain->SetBranchAddress("MatchedEMPFjets_m", &MatchedEMPFjets_m, &b_MatchedEMPFjets_m);
   fChain->SetBranchAddress("MatchedEMPFjets_r", &MatchedEMPFjets_r, &b_MatchedEMPFjets_r);
   fChain->SetBranchAddress("MatchedEMPFjets_nChargedTrack", &MatchedEMPFjets_nChargedTrack, &b_MatchedEMPFjets_nChargedTrack);
   fChain->SetBranchAddress("MatchedEMPFjets_nChargedPFO", &MatchedEMPFjets_nChargedPFO, &b_MatchedEMPFjets_nChargedPFO);
   fChain->SetBranchAddress("MatchedEMPFjets_nNeutralPFO", &MatchedEMPFjets_nNeutralPFO, &b_MatchedEMPFjets_nNeutralPFO);
   fChain->SetBranchAddress("MatchedLCTopojets_eta", &MatchedLCTopojets_eta, &b_MatchedLCTopojets_eta);
   fChain->SetBranchAddress("MatchedLCTopojets_phi", &MatchedLCTopojets_phi, &b_MatchedLCTopojets_phi);
   fChain->SetBranchAddress("MatchedLCTopojets_pt", &MatchedLCTopojets_pt, &b_MatchedLCTopojets_pt);
   fChain->SetBranchAddress("MatchedLCTopojets_e", &MatchedLCTopojets_e, &b_MatchedLCTopojets_e);
   fChain->SetBranchAddress("MatchedLCTopojets_m", &MatchedLCTopojets_m, &b_MatchedLCTopojets_m);
   fChain->SetBranchAddress("MatchedLCTopojets_r", &MatchedLCTopojets_r, &b_MatchedLCTopojets_r);
   fChain->SetBranchAddress("MatchedLCTopojets_nChargedTrack", &MatchedLCTopojets_nChargedTrack, &b_MatchedLCTopojets_nChargedTrack);
   fChain->SetBranchAddress("MatchedLCTopojets_nChargedPFO", &MatchedLCTopojets_nChargedPFO, &b_MatchedLCTopojets_nChargedPFO);
   fChain->SetBranchAddress("MatchedLCTopojets_nNeutralPFO", &MatchedLCTopojets_nNeutralPFO, &b_MatchedLCTopojets_nNeutralPFO);
   fChain->SetBranchAddress("MatchedEMPFtaus_eta", &MatchedEMPFtaus_eta, &b_MatchedEMPFtaus_eta);
   fChain->SetBranchAddress("MatchedEMPFtaus_phi", &MatchedEMPFtaus_phi, &b_MatchedEMPFtaus_phi);
   fChain->SetBranchAddress("MatchedEMPFtaus_pt", &MatchedEMPFtaus_pt, &b_MatchedEMPFtaus_pt);
   fChain->SetBranchAddress("MatchedEMPFtaus_e", &MatchedEMPFtaus_e, &b_MatchedEMPFtaus_e);
   fChain->SetBranchAddress("MatchedEMPFtaus_e_np", &MatchedEMPFtaus_e_np, &b_MatchedEMPFtaus_e_np);
   fChain->SetBranchAddress("MatchedEMPFtaus_m", &MatchedEMPFtaus_m, &b_MatchedEMPFtaus_m);
   fChain->SetBranchAddress("MatchedEMPFtaus_nChargedPion", &MatchedEMPFtaus_nChargedPion, &b_MatchedEMPFtaus_nChargedPion);
   fChain->SetBranchAddress("MatchedEMPFtaus_nNeutralPion", &MatchedEMPFtaus_nNeutralPion, &b_MatchedEMPFtaus_nNeutralPion);
   fChain->SetBranchAddress("MatchedLCTopotaus_eta", &MatchedLCTopotaus_eta, &b_MatchedLCTopotaus_eta);
   fChain->SetBranchAddress("MatchedLCTopotaus_phi", &MatchedLCTopotaus_phi, &b_MatchedLCTopotaus_phi);
   fChain->SetBranchAddress("MatchedLCTopotaus_pt", &MatchedLCTopotaus_pt, &b_MatchedLCTopotaus_pt);
   fChain->SetBranchAddress("MatchedLCTopotaus_e", &MatchedLCTopotaus_e, &b_MatchedLCTopotaus_e);
   fChain->SetBranchAddress("MatchedLCTopotaus_e_np", &MatchedLCTopotaus_e_np, &b_MatchedLCTopotaus_e_np);
   fChain->SetBranchAddress("MatchedLCTopotaus_m", &MatchedLCTopotaus_m, &b_MatchedLCTopotaus_m);
   fChain->SetBranchAddress("MatchedLCTopotaus_nChargedPion", &MatchedLCTopotaus_nChargedPion, &b_MatchedLCTopotaus_nChargedPion);
   fChain->SetBranchAddress("MatchedLCTopotaus_nNeutralPion", &MatchedLCTopotaus_nNeutralPion, &b_MatchedLCTopotaus_nNeutralPion);

}
