#include "SingleTreeRunner.h"
#include "softTau.h"
#include "easylogging++.h"
#include "AnaObjs.h"
#include "PhyUtils.h"
#include "GRLDB.h" 

//#include "XamppTree.h"
DefineLooper(softTauAna, softTau);

void softTauAna::loop()
{

	std::string outFullName = getOutName();
	std::string outTree = Utils::splitStrBy(outFullName,'.')[0] + "_Nominal";
	auto oTree = cloneCurrentAnaTree(outTree);

	auto mCutflow = addCutflow();
	//define cuts
//	Var["tau1Pt"] = 0;
	Var["n_matched_EMPF"] = 0;
	Var["n_matched_LCTopo"] = 0;
	Var["n_tau_jet"] = 0;
	Var["n_truth_tau"] = 0;
	std::vector<double> taujet_jetpt;
	std::vector<double> matched_LCTopo_jetpt;
	std::vector<double> matched_EMPF_jetpt;
	std::vector<double> LCTopo_jetpt;
	std::vector<double> EMPF_jetpt;

	std::vector<double> matched_LCTopo_taupt;
	std::vector<double> matched_LCTopo_taueta;
	std::vector<double> matched_LCTopo_taudecay;
	std::vector<double> matched_LCTopo_jetdecay;
	std::vector<double> matched_EMPF_taupt;
	std::vector<double> matched_EMPF_taueta;
	std::vector<double> matched_EMPF_taudecay;
	std::vector<double> matched_EMPF_jetdecay;
	std::vector<double> truth_taupt;
	std::vector<double> truth_taueta;
	std::vector<double> truth_taudecay;
	
	std::vector<double> matched_LCTopo_jeteta;
	std::vector<double> matched_EMPF_jeteta;
	
	std::vector<double> taujetpt;
	std::vector<double> taujeteta;
	
	std::vector<double> R_LCTopo_taupt;
	std::vector<double> R_EMPF_taupt;
	std::vector<double> R_truth_taupt;
	
	std::vector<double> mt1_LCTopo_taupt;
	std::vector<double> mt1_LCTopo_taueta;
	std::vector<double> mt1_LCTopo_taudecay;
	std::vector<double> mt1_LCTopo_jetdecay;
	std::vector<double> mt1_EMPF_taupt;
	std::vector<double> mt1_EMPF_taueta;
	std::vector<double> mt1_EMPF_taudecay;
	std::vector<double> mt1_EMPF_jetdecay;
	std::vector<double> mp1_truth_taupt;
	std::vector<double> mp1_truth_taueta;
	std::vector<double> mp1_truth_taudecay;

	std::vector<double> mt1_LCTopo_jeteta;
	std::vector<double> mt1_EMPF_jeteta;
	std::vector<double> mt1_LCTopo_jetpt;
	std::vector<double> mt1_EMPF_jetpt;

	std::vector<double> t1p1_matched_LCTopo_taupt;
	std::vector<double> t1p1_matched_LCTopo_taueta;
	std::vector<double> t1p1_matched_LCTopo_taudecay;
	std::vector<double> t1p1_matched_EMPF_taupt;
	std::vector<double> t1p1_matched_EMPF_taueta;
	std::vector<double> t1p1_matched_EMPF_taudecay;
	std::vector<double> p1_truth_taupt;
	std::vector<double> p1_truth_taueta;
	std::vector<double> p1_truth_taudecay;

	std::vector<double> t3p3_matched_LCTopo_taupt;
	std::vector<double> t3p3_matched_LCTopo_taueta;
	std::vector<double> t3p3_matched_LCTopo_taudecay;
	std::vector<double> t3p3_matched_EMPF_taupt;
	std::vector<double> t3p3_matched_EMPF_taueta;
	std::vector<double> t3p3_matched_EMPF_taudecay;
	std::vector<double> p3_truth_taupt;
	std::vector<double> p3_truth_taueta;
	std::vector<double> p3_truth_taudecay;
	
	std::vector<double> t1p1t3p3_matched_LCTopo_taupt;
	std::vector<double> t1p1t3p3_matched_LCTopo_taueta;
	std::vector<double> t1p1t3p3_matched_LCTopo_taudecay;
	std::vector<double> t1p1t3p3_matched_EMPF_taupt;
	std::vector<double> t1p1t3p3_matched_EMPF_taueta;
	std::vector<double> t1p1t3p3_matched_EMPF_taudecay;
	std::vector<double> p1p3_truth_taupt;
	std::vector<double> p1p3_truth_taueta;
	std::vector<double> p1p3_truth_taudecay;

	std::vector<double> Truth_neutralPi_taue;
	std::vector<double> EMPF_neutralPi_taue;
    std::vector<double> LCTopo_neutralPi_taue;
	
	auto mergevector = [](std::vector<double> a, std::vector<double> b) {
		a.insert(a.end(),b.begin(),b.end());
		return a;
	};

	auto pTfilter = [](double a ) {
		if ( a>20 && a<40 ) return true;
		return false;
	};
	
	auto decaymode = [](int c, int n) {
		if ( c==1 && n<=1 ){
			return 1.0*n;
		} else if ( c==1 && n>=2 ){
			return 2.;
		} else if ( c==3 && n==0 ){
			return 3.;
		} else if ( c==3 && n>=1 ){
			return 4.;
		}else{
			return 5.;
		}
	};

	auto deltavalue = [](std::vector<double> a, std::vector<double> b) {
		std::vector<double> deltaval;
		for(int i=0 ; i< a.size() ; i++){
			deltaval.push_back( a.at(i) - b.at(i) );
		}
		return deltaval;
	};
	
	auto reldeltavalue = [](std::vector<double> a, std::vector<double> b) {
		std::vector<double> deltaval;
		for(int i=0 ; i< a.size() ; i++){
			deltaval.push_back( (a.at(i) - b.at(i)) / b.at(i) );
		}
		return deltaval;
	};

	mCutflow->setWeight([&]{return 1.0;});
	mCutflow->setFillTree(oTree);
//将m_Tree导入Cutflow里的outTree
	mCutflow->registerCut("Do Nothing",[&] {return true;});
//	mCutflow->registerCut("OS", [&] {return (OS2Tau); });
	auto lastCut = mCutflow->registerCut("the END", [&] {return true; });
	
//	lastCut->addHist("nMatched_tauLCTopo", 30, 0, 30, [&] {return Var["n_matched_LCTopo"]; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("nMatched_tauEMPF", 30, 0, 30, [&] {return Var["n_matched_EMPF"]; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("nTaujet", 30, 0, 30, [&] {return Var["n_tau_jet"]; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("nTruthTau", 30, 0, 30, [&] {return Var["n_truth_tau"]; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("Matched_LCTopo_jetpt", 50, 0, 50, [&] {return matched_LCTopo_jetpt; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("Matched_EMPF_jetpt", 50, 0, 50, [&] {return matched_EMPF_jetpt; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("LCTopo_jetpt", 50, 0, 50, [&] {return LCTopo_jetpt; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("EMPF_jetpt", 50, 0, 50, [&] {return EMPF_jetpt; }, Hist::USE_OVERFLOW);
//	lastCut->addHist("Taujetpt", 50, 0, 50, [&] {return taujet_jetpt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_LCTopo_taupt", 60, 0, 60, [&] {return matched_LCTopo_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_EMPF_taupt", 60, 0, 60, [&] {return matched_EMPF_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Truth_taupt", 60, 0, 60, [&] {return truth_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_LCTopo_taueta", 50, -2.5, 2.5, [&] {return matched_LCTopo_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_EMPF_taueta", 50, -2.5, 2.5, [&] {return matched_EMPF_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Truth_taueta", 50, -2.5, 2.5, [&] {return truth_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_LCTopo_taudecay", 6, 0, 6, [&] {return matched_LCTopo_taudecay; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_EMPF_taudecay", 6, 0, 6, [&] {return matched_EMPF_taudecay; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Truth_taudecay", 6, 0, 6, [&] {return truth_taudecay; }, Hist::USE_OVERFLOW);

	lastCut->addHist("Matched_LCTopo_jeteta", 50, -2.5, 2.5, [&] {return matched_LCTopo_jeteta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("Matched_EMPF_jeteta", 50, -2.5, 2.5, [&] {return matched_EMPF_jeteta; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("MT1_LCTopo_taupt", 60, 0, 60, [&] {return mt1_LCTopo_taupt;}, Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_EMPF_taupt", 60, 0, 60,   [&] {return mt1_EMPF_taupt;}, Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_LCTopo_taueta", 50, -2.5, 2.5, [&] {return mt1_LCTopo_taueta;}, Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_EMPF_taueta", 50, -2.5, 2.5,   [&] {return mt1_EMPF_taueta;}, Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_LCTopo_taudecay", 6, 0, 6, [&] {return mt1_LCTopo_taudecay;}, Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_EMPF_taudecay", 6, 0, 6,   [&] {return mt1_EMPF_taudecay;}, Hist::USE_OVERFLOW);
	lastCut->addHist("MP1_Truth_taupt", 60, 0, 60, [&] {return mp1_truth_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("MP1_Truth_taueta", 50, -2.5, 2.5, [&] {return mp1_truth_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("MP1_Truth_taudecay", 6, 0, 6, [&] {return mp1_truth_taudecay; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("MT1_LCTopo_deltataupt", 150, -1, 2, [&] {return reldeltavalue(mt1_LCTopo_jetpt,mt1_LCTopo_taupt) ;} , Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_EMPF_deltataupt", 150, -1, 2, [&] {return reldeltavalue(mt1_EMPF_jetpt,mt1_EMPF_taupt) ;} , Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_LCTopo_deltataueta", 40, -0.2, 0.2, [&] {return deltavalue(mt1_LCTopo_jeteta,mt1_LCTopo_taueta) ;} , Hist::USE_OVERFLOW);
	lastCut->addHist("MT1_EMPF_deltataueta", 40, -0.2, 0.2, [&] {return deltavalue(mt1_EMPF_jeteta,mt1_EMPF_taueta) ;} , Hist::USE_OVERFLOW);
	
	lastCut->addHist("R_LCTopo_taupt", 60, 0, 60, [&] {return R_LCTopo_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("R_EMPF_taupt", 60, 0, 60, [&] {return R_EMPF_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("R_Truth_taupt", 60, 0, 60, [&] {return R_truth_taupt; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("T1_LCTopo_taupt", 60, 0, 60, [&] {return t1p1_matched_LCTopo_taupt;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1_LCTopo_taueta", 50, -2.5, 2.5, [&] {return t1p1_matched_LCTopo_taueta;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1_LCTopo_taudecay", 6, 0, 6, [&] {return t1p1_matched_LCTopo_taudecay;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1_EMPF_taupt", 60, 0, 60,   [&] {return t1p1_matched_EMPF_taupt;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1_EMPF_taueta", 50, -2.5, 2.5,   [&] {return t1p1_matched_EMPF_taueta;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1_EMPF_taudecay", 6, 0, 6,   [&] {return t1p1_matched_EMPF_taudecay;}, Hist::USE_OVERFLOW);
	lastCut->addHist("P1_Truth_taupt", 60, 0, 60, [&] {return p1_truth_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("P1_Truth_taueta", 50, -2.5, 2.5, [&] {return p1_truth_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("P1_Truth_taudecay", 6, 0, 6, [&] {return p1_truth_taudecay; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("T3_LCTopo_taupt", 60, 0, 60, [&] {return t3p3_matched_LCTopo_taupt;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T3_LCTopo_taueta", 50, -2.5, 2.5, [&] {return t3p3_matched_LCTopo_taueta;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T3_LCTopo_taudecay", 6, 0, 6, [&] {return t3p3_matched_LCTopo_taudecay;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T3_EMPF_taupt", 60, 0, 60,   [&] {return t3p3_matched_EMPF_taupt;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T3_EMPF_taueta", 50, -2.5, 2.5,   [&] {return t3p3_matched_EMPF_taueta;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T3_EMPF_taudecay", 6, 0, 6,   [&] {return t3p3_matched_EMPF_taudecay;}, Hist::USE_OVERFLOW);
	lastCut->addHist("P3_Truth_taupt", 60, 0, 60, [&] {return p3_truth_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("P3_Truth_taueta", 50, -2.5, 2.5, [&] {return p3_truth_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("P3_Truth_taudecay", 6, 0, 6, [&] {return p3_truth_taudecay; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("T1T3_LCTopo_taupt", 60, 0, 60, [&] {return t1p1t3p3_matched_LCTopo_taupt ;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1T3_LCTopo_taueta", 50, -2.5, 2.5, [&] {return t1p1t3p3_matched_LCTopo_taueta ;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1T3_LCTopo_taudecay", 6, 0, 6, [&] {return t1p1t3p3_matched_LCTopo_taudecay ;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1T3_EMPF_taupt", 60, 0, 60,   [&] {return t1p1t3p3_matched_EMPF_taupt ;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1T3_EMPF_taueta", 50, -2.5, 2.5,   [&] {return t1p1t3p3_matched_EMPF_taueta ;}, Hist::USE_OVERFLOW);
	lastCut->addHist("T1T3_EMPF_taudecay", 6, 0, 6,   [&] {return t1p1t3p3_matched_EMPF_taudecay ;}, Hist::USE_OVERFLOW);
	lastCut->addHist("P1P3_Truth_taupt", 60, 0, 60, [&] {return p1p3_truth_taupt; }, Hist::USE_OVERFLOW);
	lastCut->addHist("P1P3_Truth_taueta", 50, -2.5, 2.5, [&] {return p1p3_truth_taueta; }, Hist::USE_OVERFLOW);
	lastCut->addHist("P1P3_Truth_taudecay", 6, 0, 6, [&] {return p1p3_truth_taudecay ; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("np_LCTopo_taue", 10, 0, 1, [&] {return LCTopo_neutralPi_taue; }, Hist::USE_OVERFLOW);
	lastCut->addHist("np_EMPF_taue", 10, 0, 1, [&] {return EMPF_neutralPi_taue; }, Hist::USE_OVERFLOW);
	lastCut->addHist("np_Truth_taue", 10, 0, 1, [&] {return Truth_neutralPi_taue; }, Hist::USE_OVERFLOW);
	
	lastCut->addHist("TruthTau", 60, 0, 60, 50, -2.5, 2.5, [&] {return std::make_pair(truth_taupt,truth_taueta); }, false );
	lastCut->addHist("LCTopoTaujets", 60, 0, 60, 50, -2.5, 2.5, [&] {return std::make_pair(matched_LCTopo_taupt,matched_LCTopo_taueta); }, false );
	lastCut->addHist("PFlowTaujets", 60, 0, 60, 50, -2.5, 2.5, [&] {return std::make_pair(matched_EMPF_taupt,matched_EMPF_taueta); }, false );
	
	lastCut->addHist("MT1_LCTopoDecayMode", 6, 0, 6, 6, 0, 6, [&] {return std::make_pair(mt1_LCTopo_taudecay,mt1_LCTopo_jetdecay); }, false );
	lastCut->addHist("MT1_PFlowDecayMode", 6, 0, 6, 6, 0, 6, [&] {return std::make_pair(mt1_EMPF_taudecay,mt1_EMPF_jetdecay); }, false );
//Loop Start
	Long64_t nentries = tree->GetEntries();
	for (Long64_t jentry=0; jentry<nentries;jentry++) {
		tree->GetEntry(jentry);
		//if (0 == jentry % 1000)
			LOG(INFO) << jentry << " entry of " << nentries << " entries";
		matched_LCTopo_jetpt.clear();
		matched_EMPF_jetpt.clear();
		LCTopo_jetpt.clear();
		EMPF_jetpt.clear();
		matched_LCTopo_taupt.clear();
		matched_EMPF_taupt.clear();
		truth_taupt.clear();
		taujet_jetpt.clear();
		matched_LCTopo_taueta.clear();
		matched_EMPF_taueta.clear();
		truth_taueta.clear();
	
		matched_LCTopo_jeteta.clear();
		matched_EMPF_jeteta.clear();
	
		taujetpt.clear();
		taujeteta.clear();
		
		matched_LCTopo_taudecay.clear() ;
		matched_LCTopo_jetdecay.clear() ;
		matched_EMPF_taudecay.clear() ;
		matched_EMPF_jetdecay.clear() ;
		truth_taudecay.clear() ;

		R_LCTopo_taupt.clear() ;
		R_EMPF_taupt.clear() ;
		R_truth_taupt.clear() ;
		
		mt1_LCTopo_taupt.clear();
		mt1_LCTopo_taueta.clear();
		mt1_LCTopo_taudecay.clear();
		mt1_LCTopo_jetdecay.clear();
		mt1_EMPF_taupt.clear();
		mt1_EMPF_taueta.clear();
		mt1_EMPF_taudecay.clear();
		mt1_EMPF_jetdecay.clear();
		mp1_truth_taupt.clear();
		mp1_truth_taueta.clear();
		mp1_truth_taudecay.clear();

		mt1_LCTopo_jeteta.clear();
		mt1_EMPF_jeteta.clear();
		mt1_LCTopo_jetpt.clear();
		mt1_EMPF_jetpt.clear();
	
		t1p1_matched_LCTopo_taupt.clear();
		t1p1_matched_LCTopo_taueta.clear();
		t1p1_matched_LCTopo_taudecay.clear();
		t1p1_matched_EMPF_taupt.clear();
		t1p1_matched_EMPF_taueta.clear();
		t1p1_matched_EMPF_taudecay.clear();
		p1_truth_taupt.clear();
		p1_truth_taueta.clear();
		p1_truth_taudecay.clear();

		t3p3_matched_LCTopo_taupt.clear();
		t3p3_matched_LCTopo_taueta.clear();
		t3p3_matched_LCTopo_taudecay.clear();
		t3p3_matched_EMPF_taupt.clear();
		t3p3_matched_EMPF_taueta.clear();
		t3p3_matched_EMPF_taudecay.clear();
		p3_truth_taupt.clear();
		p3_truth_taueta.clear();
		p3_truth_taudecay.clear();
	
		Truth_neutralPi_taue.clear();
		EMPF_neutralPi_taue.clear();
    	LCTopo_neutralPi_taue.clear();
	
		t1p1t3p3_matched_LCTopo_taupt.clear();
		t1p1t3p3_matched_LCTopo_taueta.clear();
		t1p1t3p3_matched_LCTopo_taudecay.clear();
		t1p1t3p3_matched_EMPF_taupt.clear();
		t1p1t3p3_matched_EMPF_taueta.clear();
		t1p1t3p3_matched_EMPF_taudecay.clear();
		p1p3_truth_taupt.clear();
		p1p3_truth_taueta.clear();
		p1p3_truth_taudecay.clear();

		Var["n_matched_EMPF"] = tree->nMatchedEMPFJetNumber;
		Var["n_matched_LCTopo"] = tree->nMatchedLCTopoJetNumber;
		Var["n_tau_jet"] = tree->taujets_m->size();
		Var["n_truth_tau"] = tree->taus_m->size();

		//for ( int i=0 ; i<tree->taujets_pt->size() ; i++){
		//	taujetpt.push_back(tree->taujets_pt->at(i)/1000);
		//	taujeteta.push_back(tree->taujets_eta->at(i));
		//}
	
		LOG(INFO) << " EMPF";
		//EMPFlow
		for ( int i=0 ; i<tree->MatchedEMPFtaus_pt->size() ; i++){
			matched_EMPF_taupt.push_back(tree->MatchedEMPFtaus_pt->at(i)/1000);
			if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) ){
				matched_EMPF_taueta.push_back(tree->MatchedEMPFtaus_eta->at(i));
				matched_EMPF_jeteta.push_back(tree->MatchedEMPFjets_eta->at(i));
				matched_EMPF_taudecay.push_back( decaymode( tree->MatchedEMPFtaus_nChargedPion->at(i), tree->MatchedEMPFtaus_nNeutralPion->at(i)) );
				matched_EMPF_jetdecay.push_back( decaymode( tree->MatchedEMPFjets_nChargedPFO->at(i), tree->MatchedEMPFjets_nNeutralPFO->at(i)) );
			}
			
			if ( fabs(tree->MatchedEMPFtaus_eta->at(i))>=0.1 && fabs(tree->MatchedEMPFtaus_eta->at(i))<=1.5 )
				R_EMPF_taupt.push_back(tree->MatchedEMPFtaus_pt->at(i)/1000);
			
			// For taujet track >=1
			if ( tree->MatchedEMPFjets_nChargedTrack->at(i) >= 1 ) {
				mt1_EMPF_taupt.push_back(tree->MatchedEMPFtaus_pt->at(i)/1000);
				mt1_EMPF_jetpt.push_back(tree->MatchedEMPFjets_pt->at(i)/1000);
				if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) ){
					mt1_EMPF_taueta.push_back(tree->MatchedEMPFtaus_eta->at(i));
					mt1_EMPF_jeteta.push_back(tree->MatchedEMPFjets_eta->at(i));
					mt1_EMPF_taudecay.push_back( decaymode( tree->MatchedEMPFtaus_nChargedPion->at(i), tree->MatchedEMPFtaus_nNeutralPion->at(i)) );
					mt1_EMPF_jetdecay.push_back( decaymode( tree->MatchedEMPFjets_nChargedPFO->at(i), tree->MatchedEMPFjets_nNeutralPFO->at(i)) );
				}
			}
			
			// For taujet track ==1 && truth tau p ==1
			if ( tree->MatchedEMPFjets_nChargedTrack->at(i) == 1 && tree->MatchedEMPFtaus_nChargedPion->at(i)==1 ) {
				t1p1_matched_EMPF_taupt.push_back(tree->MatchedEMPFtaus_pt->at(i)/1000);
				if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) )
				t1p1_matched_EMPF_taueta.push_back(tree->MatchedEMPFtaus_eta->at(i));
				if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) )
				t1p1_matched_EMPF_taudecay.push_back( decaymode(tree->MatchedEMPFtaus_nChargedPion->at(i), tree->MatchedEMPFtaus_nNeutralPion->at(i)) );
			}
			
			// For taujet track ==3 && truth tau p ==3
			if ( tree->MatchedEMPFjets_nChargedTrack->at(i) == 3 && tree->MatchedEMPFtaus_nChargedPion->at(i)==3 ) {
				t3p3_matched_EMPF_taupt.push_back(tree->MatchedEMPFtaus_pt->at(i)/1000);
				if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) )
				t3p3_matched_EMPF_taueta.push_back(tree->MatchedEMPFtaus_eta->at(i));
				if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) )
				t3p3_matched_EMPF_taudecay.push_back( decaymode(tree->MatchedEMPFtaus_nChargedPion->at(i), tree->MatchedEMPFtaus_nNeutralPion->at(i)) );
				if ( pTfilter(tree->MatchedEMPFtaus_pt->at(i)/1000) )
				EMPF_neutralPi_taue.push_back(tree->MatchedEMPFtaus_e_np->at(i)/tree->MatchedEMPFtaus_e->at(i) );
			}
		}

		LOG(INFO) << " LCTopo";
		//LCTopo
		for ( int i=0 ; i<tree->MatchedLCTopotaus_pt->size() ; i++){
			matched_LCTopo_taupt.push_back(tree->MatchedLCTopotaus_pt->at(i)/1000);
			if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) ){
				matched_LCTopo_taueta.push_back(tree->MatchedLCTopotaus_eta->at(i));
				matched_LCTopo_jeteta.push_back(tree->MatchedLCTopojets_eta->at(i));
				matched_LCTopo_taudecay.push_back( decaymode(tree->MatchedLCTopotaus_nChargedPion->at(i), tree->MatchedLCTopotaus_nNeutralPion->at(i)) );
				matched_LCTopo_jetdecay.push_back( decaymode(tree->MatchedLCTopojets_nChargedPFO->at(i), tree->MatchedLCTopojets_nNeutralPFO->at(i)) );
			}
			
			if ( fabs(tree->MatchedLCTopotaus_eta->at(i))>=0.1 && fabs(tree->MatchedLCTopotaus_eta->at(i))<=1.5 )
				R_LCTopo_taupt.push_back(tree->MatchedLCTopotaus_pt->at(i)/1000);
			
			// For taujet track >=1
			if ( tree->MatchedLCTopojets_nChargedTrack->at(i) >= 1 ){
				mt1_LCTopo_taupt.push_back(tree->MatchedLCTopotaus_pt->at(i)/1000);
				mt1_LCTopo_jetpt.push_back(tree->MatchedLCTopojets_pt->at(i)/1000);
				if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) ) {
					mt1_LCTopo_taueta.push_back(tree->MatchedLCTopotaus_eta->at(i));
					mt1_LCTopo_jeteta.push_back(tree->MatchedLCTopojets_eta->at(i));
					mt1_LCTopo_taudecay.push_back( decaymode(tree->MatchedLCTopotaus_nChargedPion->at(i), tree->MatchedLCTopotaus_nNeutralPion->at(i)) );
					mt1_LCTopo_jetdecay.push_back( decaymode(tree->MatchedLCTopojets_nChargedPFO->at(i), tree->MatchedLCTopojets_nNeutralPFO->at(i)) );
				}
			}
			
			// For taujet track ==1 && truth tau p ==1
			if ( tree->MatchedLCTopojets_nChargedTrack->at(i) == 1 && tree->MatchedLCTopotaus_nChargedPion->at(i)==1 ){
				t1p1_matched_LCTopo_taupt.push_back(tree->MatchedLCTopotaus_pt->at(i)/1000);
				if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) )
				t1p1_matched_LCTopo_taueta.push_back(tree->MatchedLCTopotaus_eta->at(i));
				if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) )
				t1p1_matched_LCTopo_taudecay.push_back( decaymode(tree->MatchedLCTopotaus_nChargedPion->at(i), tree->MatchedLCTopotaus_nNeutralPion->at(i)) );
			}
			
			// For taujet track ==3 && truth tau p ==3
			if ( tree->MatchedLCTopojets_nChargedTrack->at(i) == 3 && tree->MatchedLCTopotaus_nChargedPion->at(i) == 3 ){
				t3p3_matched_LCTopo_taupt.push_back(tree->MatchedLCTopotaus_pt->at(i)/1000);
				if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) )
				t3p3_matched_LCTopo_taueta.push_back(tree->MatchedLCTopotaus_eta->at(i));
				if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) )
				t3p3_matched_LCTopo_taudecay.push_back( decaymode(tree->MatchedLCTopotaus_nChargedPion->at(i), tree->MatchedLCTopotaus_nNeutralPion->at(i)) );
				if ( pTfilter(tree->MatchedLCTopotaus_pt->at(i)/1000) )
				LCTopo_neutralPi_taue.push_back(tree->MatchedLCTopotaus_e_np->at(i)/tree->MatchedLCTopotaus_e->at(i) );
			}
		}

		LOG(INFO) << "Truth";
		/***************************************/
		// Truth tau
		for ( int i=0 ; i<tree->taus_pt->size() ; i++){
			truth_taupt.push_back(tree->taus_pt->at(i)/1000);
			if ( pTfilter(tree->taus_pt->at(i)/1000) ) {
				truth_taueta.push_back(tree->taus_eta->at(i));
				truth_taudecay.push_back( decaymode(tree->taus_nChargedPion->at(i), tree->taus_nNeutralPion->at(i)) );
			}
			
			if ( fabs(tree->taus_eta->at(i))>=0.1 && fabs(tree->taus_eta->at(i))<=1.5 )
				R_truth_taupt.push_back(tree->taus_pt->at(i)/1000);
			
			// For truth tau prong >=1
			if( tree->taus_nChargedPion->at(i)>=1 ){
				mp1_truth_taupt.push_back(tree->taus_pt->at(i)/1000);
				if ( pTfilter(tree->taus_pt->at(i)/1000) ) {
					mp1_truth_taueta.push_back(tree->taus_eta->at(i));
					mp1_truth_taudecay.push_back( decaymode(tree->taus_nChargedPion->at(i), tree->taus_nNeutralPion->at(i)) );
				}
			}

			// For taujet track ==1 && truth tau p ==1
			if ( tree->taus_nChargedPion->at(i)==1 ){
				p1_truth_taupt.push_back(tree->taus_pt->at(i)/1000);
				if ( pTfilter(tree->taus_pt->at(i)/1000) )
				p1_truth_taueta.push_back(tree->taus_eta->at(i));
				if ( pTfilter(tree->taus_pt->at(i)/1000) )
				p1_truth_taudecay.push_back( decaymode(tree->taus_nChargedPion->at(i), tree->taus_nNeutralPion->at(i)) );
			}
			
			// For taujet track ==3 && truth tau p ==3
			if ( tree->taus_nChargedPion->at(i) == 3 ){
				p3_truth_taupt.push_back(tree->taus_pt->at(i)/1000);
				if ( pTfilter(tree->taus_pt->at(i)/1000) )
				p3_truth_taueta.push_back(tree->taus_eta->at(i));
				if ( pTfilter(tree->taus_pt->at(i)/1000) )
				p3_truth_taudecay.push_back( decaymode(tree->taus_nChargedPion->at(i), tree->taus_nNeutralPion->at(i)) );
				if ( pTfilter(tree->taus_pt->at(i)/1000) )
				Truth_neutralPi_taue.push_back(tree->taus_e_np->at(i)/tree->taus_e->at(i) );
			}
		}
		
		t1p1t3p3_matched_LCTopo_taupt   = mergevector(t1p1_matched_LCTopo_taupt   ,t3p3_matched_LCTopo_taupt   );
		t1p1t3p3_matched_LCTopo_taueta  = mergevector(t1p1_matched_LCTopo_taueta  ,t3p3_matched_LCTopo_taueta  );
		t1p1t3p3_matched_LCTopo_taudecay= mergevector(t1p1_matched_LCTopo_taudecay,t3p3_matched_LCTopo_taudecay);
		t1p1t3p3_matched_EMPF_taupt   = mergevector(t1p1_matched_EMPF_taupt   ,t3p3_matched_EMPF_taupt   );
		t1p1t3p3_matched_EMPF_taueta  = mergevector(t1p1_matched_EMPF_taueta  ,t3p3_matched_EMPF_taueta  );
		t1p1t3p3_matched_EMPF_taudecay= mergevector(t1p1_matched_EMPF_taudecay,t3p3_matched_EMPF_taudecay);
		p1p3_truth_taupt   = mergevector(p1_truth_taupt   ,p3_truth_taupt   );
		p1p3_truth_taueta  = mergevector(p1_truth_taueta  ,p3_truth_taueta  );
		p1p3_truth_taudecay= mergevector(p1_truth_taudecay,p3_truth_taudecay);
		
		LOG(INFO) << "end";
		
		fillRegions();
		fillCutflows();
	}
	for(auto cut : getCutflows()){
		cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName,'.')[0]);
	}
}

