pathditaumet="../../run/"

#mkdir build
#cd build && rm -rf *
#cmake ../

cd build
make -j 8

#name=("PFlowTaujets PFlowTaujets PFlowTaujets 2" )
#name=("LCTopoTaujets LCTopoTaujets LCTopoTaujets 2" )
#name=( "TruthTau TruthTau TruthTau 2" )
# "P3_Truth_taupt T3_LCTopo_taupt T3_EMPF_taupt 1"\
# "P3_Truth_taueta T3_LCTopo_taueta T3_EMPF_taueta 1"\
# "P3_Truth_taudecay T3_LCTopo_taudecay T3_EMPF_taudecay 1"\
# "P1_Truth_taupt    T1_LCTopo_taupt    T1_EMPF_taupt 1"\
# "P1_Truth_taueta   T1_LCTopo_taueta   T1_EMPF_taueta 1"\
# "P1_Truth_taudecay T1_LCTopo_taudecay T1_EMPF_taudecay 1"\
# "MP1_Truth_taupt MT1_LCTopo_taupt MT1_EMPF_taupt 1"\
# "MP1_Truth_taueta MT1_LCTopo_taueta MT1_EMPF_taueta 1"\
# "MP1_Truth_taudecay MT1_LCTopo_taudecay MT1_EMPF_taudecay 1"\
#name=(\
#)
# "MT1_LCTopo_deltataupt MT1_EMPF_deltataupt 0"\
# "MT1_LCTopo_deltataueta MT1_EMPF_deltataueta 0"\
# "Matched_EMPF_taueta Matched_EMPF_jeteta 0"\
# "Matched_LCTopo_taueta Matched_LCTopo_jeteta 0"\
# "Matched_LCTopo_taupt Matched_EMPF_taupt Truth_taupt 0"\
# "np_Truth_taue 0"\
# "Truth_taupt 0"\
# "Truth_taueta 0"\
# "Truth_taudecay 0"\
# "Matched_EMPF_taueta Matched_EMPF_jeteta 0"\
# "Matched_LCTopo_taueta Matched_LCTopo_jeteta 0"\
# "P3_Truth_taupt T3_LCTopo_taupt T3_EMPF_taupt 1"\
# "P3_Truth_taueta T3_LCTopo_taueta T3_EMPF_taueta 1"\
# "P3_Truth_taudecay T3_LCTopo_taudecay T3_EMPF_taudecay 1"\
# "P1_Truth_taupt T1_LCTopo_taupt T1_EMPF_taupt 1"\
# "P1_Truth_taueta T1_LCTopo_taueta T1_EMPF_taueta 1"\
# "P1_Truth_taudecay T1_LCTopo_taudecay T1_EMPF_taudecay 1"\
# "P1P3_Truth_taupt T1T3_LCTopo_taupt T1T3_EMPF_taupt 1"\
# "P1P3_Truth_taueta T1T3_LCTopo_taueta T1T3_EMPF_taueta 1"\
# "P1P3_Truth_taudecay T1T3_LCTopo_taudecay T1T3_EMPF_taudecay 1"\
# "Truth_taupt MT1_LCTopo_taupt MT1_EMPF_taupt 1"\
# "Truth_taueta MT1_LCTopo_taueta MT1_EMPF_taueta 1"\
# "Truth_taudecay MT1_LCTopo_taudecay MT1_EMPF_taudecay 1"\
# "R_Truth_taupt R_LCTopo_taupt R_EMPF_taupt 1"\
# "Truth_taupt Matched_LCTopo_taupt Matched_EMPF_taupt 1"\
# "Truth_taueta Matched_LCTopo_taueta Matched_EMPF_taueta 1"\
# "Truth_taudecay Matched_LCTopo_taudecay Matched_EMPF_taudecay 1"\
# "np_Truth_taue np_LCTopo_taue np_EMPF_taue 1"\
# "MT1 MT1_LCTopoDecayMode MT1_PFlowDecayMode 2"\
name=(\
 "np_Truth_taue np_LCTopo_taue np_EMPF_taue 1"\
)

#cd ../ditaumet
cd ../ditaumet && rm -rf *
cp ../build/Ploter .

for ((i = 0; i < ${#name[@]}; i++))
do
	names=(${name[$i]})
	./Ploter "${name[$i]}"
	#echo "${names[0]}" "${names[1]}" "${names[2]}" "${names[3]}"
done
cd ..
