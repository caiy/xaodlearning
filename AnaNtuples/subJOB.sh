ana_name=("softTauAna")
selected=${ana_name[0]}
#
#cd build/ && rm -rf *
#cmake ../
cd build
make -j 8
cd ../run/ && rm -rf *
cp ../build/mini_analysis ./
#
path1="/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/ROOTAnalysisTutorial/subjob/"

#cd run
folder_without_sub=`ls $path1 | grep -v sub | grep -v job`

num=0
for i in ${folder_without_sub}
do
	if [ -d "${path1}${i}/data-ANALYSIS" ]
	then
		root_file=`ls ${path1}${i}/data-ANALYSIS | grep root `
		echo ./mini_analysis -c ../config/config.json -a ${selected} -i ${path1}${i}/data-ANALYSIS/${root_file} -o xAOD.${num}.root > sub.sh."$num"
		chmod u+x sub.sh."$num"
		nohup sh sub.sh."$num" &
		let num++
	fi
done	

#num=0
#for i in ${folder_without_sub}
#do
#	if [ -d "${path1}${i}/data-ANALYSIS" ]
#	then
#		root_file=`ls ${path1}${i}/data-ANALYSIS | grep root `
#		echo ./mini_analysis -c ../config/config.json -a ${selected} -i ${path1}${i}/data-ANALYSIS/${root_file} -o xAOD.${num}.root > sub.sh."$num"
#		chmod u+x sub.sh."$num"
#		break
#		#let num++
#	fi
#done	
#sh sub.sh."$num" 

#echo ${path1}${i}/data-ANALYSIS
#i=0
#for file in $datas
#do
#	echo ./mini_analysis -c ../config/config.json -a ${selected} -i $path2/$file -o Data.root > sub.sh."$i"
#	chmod u+x sub.sh."$i"
#	let i++
#done
#
#total=""
#for file in $MultiBosons
#do
#	if [ "$total" = "" ];then
#		total="$path1/$file"
#	else
#		total="$total","$path1/$file"
#	fi  
#done
#echo ./mini_analysis -c ../config/config.json -a ${selected} -i "$total" -o MultiBoson.root > sub.sh."$i"
#chmod u+x sub.sh."$i"
#let i++

#hep_sub -os SL7 sub.sh."%{ProcId}" -g atlas -n "$num"

cd ../
