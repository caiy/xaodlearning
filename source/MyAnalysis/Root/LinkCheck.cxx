#include <MyAnalysis/LinkCheck.h>
#include <xAODEventInfo/EventInfo.h>
#include <AthLinks/ElementLink.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCore/AuxContainerBase.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

#include "MyAnalysis/Utils.h"

LinkCheck::LinkCheck(const std::string& name,
		ISvcLocator *pSvcLocator)
	: EL::AnaAlgorithm (name, pSvcLocator)
{
	//init variables
    //#include <AsgTools/MessageCheck.h>
    // declare the tool handle as a property on the algorithm
	//declareProperty ("grlTool", m_grl, "the GRL tool");
}

StatusCode LinkCheck::initialize()
{
	//Tool loaded?
	//ANA_CHECK (m_grl.retrieve());
	
	//histogram and ...
	//ANA_MSG_INFO ("in initialize");
	
	//ntuple
	ANA_CHECK (book (TTree ("analysis", "analysis ntuple")));
	TTree* mytree = tree ("analysis");
	ANA_CHECK (book (TH1F ("Matched_TauPt", "Matched_TauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Truth_tauPt", "Truth_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Matched_JetTau_Pt", "Matched_JetTau_Pt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("HadTau_DecayMode", "HadTau_DecayMode", 15, 0, 15))); // 5*(charged_pion-1)+neutral_pion max:5*(3-1)+4

    m_nMatched = 0;
	m_EMPFjetEta = new std::vector<double>();
	m_EMPFjetPhi = new std::vector<double>();
	m_EMPFjetPt  = new std::vector<double>();
	m_EMPFjetE   = new std::vector<double>();
	m_LCTopojetEta = new std::vector<double>();
	m_LCTopojetPhi = new std::vector<double>();
	m_LCTopojetPt  = new std::vector<double>();
	m_LCTopojetE   = new std::vector<double>();
	m_LCTopojetM   = new std::vector<double>();
	m_tauNum = new std::vector<int>();
	m_tauEta = new std::vector<double>();
	m_tauPhi = new std::vector<double>();
	m_tauPt  = new std::vector<double>();
	m_tauE   = new std::vector<double>();
	m_tauM   = new std::vector<double>();
	m_tauE_NPion = new std::vector<double>();
	m_ChargedPion = new std::vector<size_t>();
	m_NeutralPion = new std::vector<size_t>();
	
	m_tauJetEta = new std::vector<double>();
	m_tauJetPhi = new std::vector<double>();
	m_tauJetPt  = new std::vector<double>();
	m_tauJetE   = new std::vector<double>();
	m_tauJetM   = new std::vector<double>();
	
    m_MatchedjetEta = new std::vector<double>();
	m_MatchedjetPhi = new std::vector<double>();
	m_MatchedjetPt  = new std::vector<double>();
	m_MatchedjetE   = new std::vector<double>();
	m_MatchedjetM   = new std::vector<double>();
	m_MatchedtauEta = new std::vector<double>();
	m_MatchedtauPhi = new std::vector<double>();
	m_MatchedtauPt  = new std::vector<double>();
	m_MatchedtauE   = new std::vector<double>();
	m_MatchedtauM   = new std::vector<double>();
	
    m_MatchedJetTauEta = new std::vector<double>();
	m_MatchedJetTauPhi = new std::vector<double>();
	m_MatchedJetTauPt  = new std::vector<double>();
	m_MatchedJetTauE   = new std::vector<double>();
	m_MatchedJetTauM   = new std::vector<double>();
    
	mytree->Branch ("RunNumber", &m_runNumber);
	mytree->Branch ("EventNumber", &m_eventNumber);
	
    mytree->Branch ("EMPFjets_eta", &m_EMPFjetEta);
	mytree->Branch ("EMPFjets_phi", &m_EMPFjetPhi);
	mytree->Branch ("EMPFjets_pt" , &m_EMPFjetPt );
	mytree->Branch ("EMPFjets_e"  , &m_EMPFjetE  );
	mytree->Branch ("EMPFjets_m"  , &m_EMPFjetM  );
	mytree->Branch ("LCTopojets_eta", &m_LCTopojetEta);
	mytree->Branch ("LCTopojets_phi", &m_LCTopojetPhi);
	mytree->Branch ("LCTopojets_pt" , &m_LCTopojetPt );
	mytree->Branch ("LCTopojets_e"  , &m_LCTopojetE  );
	mytree->Branch ("LCTopojets_m"  , &m_LCTopojetM  );
	
    mytree->Branch ("taus_number", &m_tauNum);
	mytree->Branch ("taus_eta", &m_tauEta);
	mytree->Branch ("taus_phi", &m_tauPhi);
	mytree->Branch ("taus_pt" , &m_tauPt );
	mytree->Branch ("taus_e"  , &m_tauE  );
	mytree->Branch ("taus_e_np"  , &m_tauE_NPion  );
	mytree->Branch ("taus_m"  , &m_tauM  );
	mytree->Branch ("taus_nChargedPion"  , &m_ChargedPion );//
	mytree->Branch ("taus_nNeutralPion"  , &m_NeutralPion );//
	//mytree->Branch ("taus_r"  , &m_tauR  );
	mytree->Branch ("taujet_eta", &m_tauJetEta);
	mytree->Branch ("taujet_phi", &m_tauJetPhi);
	mytree->Branch ("taujet_pt" , &m_tauJetPt );
	mytree->Branch ("taujet_e"  , &m_tauJetE  );
	mytree->Branch ("taujet_m"  , &m_tauJetM  );
	//mytree->Branch ("ele_r"  , &m_EleR  );
	
    mytree->Branch ("MatchedNum"  , &m_nMatched );
	//mytree->Branch ("MatchedEle_r"  , &m_MatchedEMPFjetR  );
	//mytree->Branch ("MatchedEle_nChargedTrack" , &m_EMPFChargedTrack);//
	//mytree->Branch ("MatchedEle_nChargedPFO" , &m_EMPFChargedPFO);//
	//mytree->Branch ("MatchedEle_nNeutralPFO" , &m_EMPFNeutralPFO);//
	mytree->Branch ("MatchedJetsTaus_eta", &m_MatchedJetTauEta);
	mytree->Branch ("MatchedJetsTaus_phi", &m_MatchedJetTauPhi);
	mytree->Branch ("MatchedJetsTaus_pt" , &m_MatchedJetTauPt );
	mytree->Branch ("MatchedJetsTaus_e"  , &m_MatchedJetTauE  );
	mytree->Branch ("MatchedJetsTaus_m"  , &m_MatchedJetTauM  );
	//mytree->Branch ("MatchedEMPFtaus_r"  , &m_MatchedEMPFtauR  );
	mytree->Branch ("MatchedJets_eta", &m_MatchedjetEta);
	mytree->Branch ("MatchedJets_phi", &m_MatchedjetPhi);
	mytree->Branch ("MatchedJets_pt" , &m_MatchedjetPt );
	mytree->Branch ("MatchedJets_e"  , &m_MatchedjetE  );
	mytree->Branch ("MatchedJets_m"  , &m_MatchedjetM  );
	mytree->Branch ("MatchedTaus_eta", &m_MatchedtauEta);
	mytree->Branch ("MatchedTaus_phi", &m_MatchedtauPhi);
	mytree->Branch ("MatchedTaus_pt" , &m_MatchedtauPt );
	mytree->Branch ("MatchedTaus_e"  , &m_MatchedtauE  );
	mytree->Branch ("MatchedTaus_m"  , &m_MatchedtauM  );
	
	return StatusCode::SUCCESS;
}

StatusCode LinkCheck::execute()
{

	m_runNumber = 0;
	m_eventNumber = 0;
	
    //****************************************************************************
	//*** Attention! If you loop over the Truth particles / vertices directly, ***
	//*** you will get signal and pile-up together. So we use the Truth event. ***
	//****************************************************************************
	m_tauNum->clear();
	m_tauEta->clear();
	m_tauPhi->clear();
	m_tauPt->clear();
	m_tauE->clear();
	m_tauE_NPion->clear();
	m_tauM->clear();
	m_ChargedPion->clear();
	m_NeutralPion->clear();
	const xAOD::TruthEventContainer* xTruthEvtContainer = NULL;
	std::vector<const xAOD::TruthParticle*> xTruthTauContainer;
	//const xAOD::TruthParticleContainer* xTruthPContainer = NULL;
	ANA_CHECK (evtStore()->retrieve (xTruthEvtContainer, "TruthEvents"));
	for (auto truthEvt : *xTruthEvtContainer) {
		const std::vector<float> weights = truthEvt->weights();
		//float scale = truthEvt->eventScale();
		for (const auto& truthPLink : truthEvt->truthParticleLinks() ){
			if (!truthPLink.isValid()) continue;
			const xAOD::TruthParticle* truthP = *truthPLink;
			if (!truthP->isTau()) continue;
			Utils::RecoTauVisDecay( *truthP ); 
			if ( truthP->auxdata<int>("IsTauDecay") < 1 ) continue;
			if ( truthP->auxdata<int>("IsHadronicTau") < 1 ) continue;
			if ( Utils::PassCaloShape(truthP->auxdata<double>("eta_vis")) ) 
			{
				m_tauEta->push_back( truthP->auxdata<double>("eta_vis") );
				m_tauPhi->push_back( truthP->auxdata<double>("phi_vis") );
				m_tauPt ->push_back( truthP->auxdata<double>("pt_vis") );
				m_tauM  ->push_back( truthP->auxdata<double>("m_vis") );
				m_tauE  ->push_back( truthP->auxdata<double>("e_vis") );
				m_tauE_NPion  ->push_back( truthP->auxdata<double>("e_vis_neutral_pions") );
				m_ChargedPion->push_back(truthP->auxdata<size_t>("numChargedPion"));
				m_NeutralPion->push_back(truthP->auxdata<size_t>("numNeutralPion"));
				hist("HadTau_DecayMode")->Fill( (truthP->auxdata<size_t>("numChargedPion")-1)*5 + truthP->auxdata<size_t>("numNeutralPion") );
				hist("Truth_tauPt")->Fill( truthP->auxdata<double>("pt_vis") * 0.001 );
				xTruthTauContainer.push_back(truthP);
			}
		}
	}
	m_tauNum->push_back(m_tauEta->size());
	
    
	std::vector<const xAOD::IParticle*> vEMPF;
    const xAOD::JetContainer* EMPFjets = nullptr;
	ANA_CHECK (evtStore()->retrieve (EMPFjets, "AntiKt4EMPFlowJets"));
	m_EMPFjetEta->clear();
	m_EMPFjetPhi->clear();
	m_EMPFjetPt->clear();
	m_EMPFjetE->clear();
	m_EMPFjetM->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *EMPFjets) {
		if ( Utils::PassCaloShape(jet->eta()) ) {
    		m_EMPFjetEta->push_back( jet->eta() );
    		m_EMPFjetPhi->push_back( jet->phi() );
    		m_EMPFjetPt->push_back( jet->pt() );
    		m_EMPFjetE->push_back( jet->e() );
    		m_EMPFjetM->push_back( jet->m() );
    	    vEMPF.push_back( jet );
        }
    }
	// LCtopo jets cont
	std::vector<const xAOD::IParticle*> vLCTopo;
	const xAOD::JetContainer* LCTopojets = nullptr;
	ANA_CHECK (evtStore()->retrieve (LCTopojets, "AntiKt4LCTopoJets"));
	m_LCTopojetEta->clear();
	m_LCTopojetPhi->clear();
	m_LCTopojetPt->clear();
	m_LCTopojetE->clear();
	m_LCTopojetM->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *LCTopojets) {
		if ( Utils::PassCaloShape(jet->eta()) ) {
		    m_LCTopojetEta->push_back( jet->eta() );
		    m_LCTopojetPhi->push_back( jet->phi() );
		    m_LCTopojetPt->push_back( jet->pt() );
		    m_LCTopojetE->push_back( jet->e() );
		    m_LCTopojetM->push_back( jet->m() );
	        vLCTopo.push_back( jet );
        }
    }


	const xAOD::TauJetContainer* taus = nullptr;
	std::vector<const xAOD::IParticle*> vTauJet;
	ANA_CHECK (evtStore()->retrieve (taus, "TauJets"));
	m_tauJetEta ->clear();
	m_tauJetPhi ->clear();
	m_tauJetPt  ->clear();
	m_tauJetE   ->clear();
	m_tauJetM   ->clear();
	for (auto taujet : *taus) {
		if ( Utils::PassCaloShape(taujet->eta()) ) {
		    m_tauJetEta->push_back( taujet->eta() );
		    m_tauJetPhi->push_back( taujet->phi() );
		    m_tauJetPt->push_back(  taujet->pt() );
		    m_tauJetE->push_back(   taujet->e() );
		    m_tauJetM->push_back(   taujet->m() );
		    vTauJet.push_back( taujet );
        }
	}
    
    
    auto match = Utils::findMatchDRI(vTauJet,xTruthTauContainer,0.2);
    //auto match = Utils::findMatchDRI(vLCTopo,xTruthTauContainer,0.2);
    //auto match = Utils::findMatchDRI(vEMPF,xTruthTauContainer,0.2);
	m_MatchedjetEta->clear();
	m_MatchedjetPhi->clear();
	m_MatchedjetPt->clear();
	m_MatchedjetE->clear();
	m_MatchedjetM->clear();
	m_MatchedtauEta->clear();
	m_MatchedtauPhi->clear();
	m_MatchedtauPt->clear();
	m_MatchedtauE->clear();
	m_MatchedtauM->clear();
	m_nMatched = match.size();
	std::vector<const xAOD::TruthParticle*> vTruthTau;
	for ( auto taujetsjet : match ) {
		if ( Utils::PassCaloShape(taujetsjet.second.second->eta()) && Utils::PassCaloShape(taujetsjet.second.first->auxdata<double>("eta_vis")) ) {
			hist("Matched_TauPt")->Fill(taujetsjet.second.first->pt() * 0.001 );
			m_MatchedjetEta->push_back( taujetsjet.second.second->eta() );
			m_MatchedjetPhi->push_back( taujetsjet.second.second->phi() );
			m_MatchedjetPt ->push_back( taujetsjet.second.second->pt() );
			m_MatchedjetE -> push_back( taujetsjet.second.second->e() );
			m_MatchedjetM -> push_back( taujetsjet.second.second->m() );
			m_MatchedtauEta->push_back( taujetsjet.second.first->auxdata<double>("eta_vis") );
			m_MatchedtauPhi->push_back( taujetsjet.second.first->auxdata<double>("phi_vis") );
			m_MatchedtauPt ->push_back( taujetsjet.second.first->auxdata<double>("pt_vis" ) );
			m_MatchedtauE -> push_back( taujetsjet.second.first->auxdata<double>("e_vis")   );
			m_MatchedtauM -> push_back( taujetsjet.second.first->auxdata<double>("m_vis")   );
		    vTruthTau.push_back(taujetsjet.second.first);
		}
	}
	
	
	//tau jet match test
	//auto TauEleMatch = Utils::findMatchDRI(vTauJet, vTruthTau,0.2);
	//auto TauEleMatch = Utils::findMatchDRI(vEMPF, vTruthTau,0.2);
	auto TauEleMatch = Utils::findMatchDRI(vLCTopo, vTruthTau,0.2);
	m_MatchedJetTauEta->clear();
	m_MatchedJetTauPhi->clear();
	m_MatchedJetTauPt->clear();
	m_MatchedJetTauE->clear();
	m_MatchedJetTauM->clear();
	for ( auto tauele : TauEleMatch ) {
		if ( Utils::PassCaloShape(tauele.second.second->eta()) && Utils::PassCaloShape(tauele.second.first->auxdata<double>("eta_vis")) ) {
			hist("Matched_JetTau_Pt")->Fill(tauele.second.first->pt() * 0.001 );
			m_MatchedJetTauEta->push_back( tauele.second.first->auxdata<double>("eta_vis") );
			m_MatchedJetTauPhi->push_back( tauele.second.first->auxdata<double>("phi_vis") );
			m_MatchedJetTauPt ->push_back( tauele.second.first->auxdata<double>("pt_vis" ) );
			m_MatchedJetTauE -> push_back( tauele.second.first->auxdata<double>("e_vis")   );
			m_MatchedJetTauM -> push_back( tauele.second.first->auxdata<double>("m_vis")   );
        }
	}
    
	//auto testmatch = Utils::findMatchDRI(vEMPF , vTauJet , 0.2);
    //if( testmatch.size() != vTauJet.size() ){
	//    ANA_MSG_INFO ("bad bad");
    //}

	// last!
	tree ("analysis")->Fill ();
	
	return StatusCode::SUCCESS;
}

StatusCode LinkCheck::finalize()
{
	//write
	return StatusCode::SUCCESS;
}


LinkCheck::~LinkCheck()
{
	delete m_EMPFjetEta;
	delete m_EMPFjetPhi;
	delete m_EMPFjetPt ;
	delete m_EMPFjetE  ;
	delete m_EMPFjetM  ;
	delete m_LCTopojetEta;
	delete m_LCTopojetPhi;
	delete m_LCTopojetPt ;
	delete m_LCTopojetE  ;
	delete m_LCTopojetM  ;
	
    delete m_tauNum ;
	delete m_tauEta ;
	delete m_tauPhi ;
	delete m_tauPt  ;
	delete m_tauE   ;
	delete m_tauE_NPion ;
	delete m_tauM   ;
	delete m_ChargedPion ;
	delete m_NeutralPion ;
	 
	delete m_tauJetEta ;
	delete m_tauJetPhi ;
	delete m_tauJetPt  ;
	delete m_tauJetE   ;
	delete m_tauJetM   ;
	
    delete m_MatchedjetEta;
	delete m_MatchedjetPhi;
	delete m_MatchedjetPt ;
	delete m_MatchedjetE  ;
	delete m_MatchedjetM  ;
    
    delete m_MatchedtauEta;
	delete m_MatchedtauPhi;
	delete m_MatchedtauPt ;
	delete m_MatchedtauE  ;
	delete m_MatchedtauM  ;

	delete m_MatchedJetTauEta ;
	delete m_MatchedJetTauPhi ;
	delete m_MatchedJetTauPt  ;
	delete m_MatchedJetTauE   ;
	delete m_MatchedJetTauM   ;
	
}
