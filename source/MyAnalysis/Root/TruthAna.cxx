#include <MyAnalysis/TruthAna.h>
#include <xAODEventInfo/EventInfo.h>
#include <AthLinks/ElementLink.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCore/AuxContainerBase.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

#include "MyAnalysis/Utils.h"

TruthAna::TruthAna(const std::string& name,
		ISvcLocator *pSvcLocator)
	: EL::AnaAlgorithm (name, pSvcLocator)
{
	//init variables
    //#include <AsgTools/MessageCheck.h>
    // declare the tool handle as a property on the algorithm
	//declareProperty ("grlTool", m_grl, "the GRL tool");
}

StatusCode TruthAna::initialize()
{
	//Tool loaded?
	//ANA_CHECK (m_grl.retrieve());
	
	//histogram and ...
	//ANA_MSG_INFO ("in initialize");
	
	//ntuple
	ANA_CHECK (book (TTree ("analysis", "analysis ntuple")));
	TTree* mytree = tree ("analysis");
	ANA_CHECK (book (TH1F ("EMPF_jetPt", "EMPFlow_jetPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("LCTopo_jetPt", "LCTopo_jetPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("MatchedEMPF_jetPt", "MatchedEMPFlow_jetPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("MatchedLCTopo_jetPt", "MatchedLCTopo_jetPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Truth_tauPt", "Truth_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("MatchedEMPF_tauPt", "MatchedEMPFlow_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("MatchedLCTopo_tauPt", "MatchedLCTopo_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("HadTau_DecayMode", "HadTau_DecayMode", 15, 0, 15))); // 5*(charged_pion-1)+neutral_pion max:5*(3-1)+4

	m_EMPFjetEta = new std::vector<double>();
	m_EMPFjetPhi = new std::vector<double>();
	m_EMPFjetPt  = new std::vector<double>();
	m_EMPFjetE   = new std::vector<double>();
	m_EMPFjetR   = new std::vector<double>();
	m_LCTopojetEta = new std::vector<double>();
	m_LCTopojetPhi = new std::vector<double>();
	m_LCTopojetPt  = new std::vector<double>();
	m_LCTopojetE   = new std::vector<double>();
	m_LCTopojetM   = new std::vector<double>();
	m_LCTopojetR   = new std::vector<double>();
	m_tauNum = new std::vector<int>();
	m_tauEta = new std::vector<double>();
	m_tauPhi = new std::vector<double>();
	m_tauPt  = new std::vector<double>();
	m_tauE   = new std::vector<double>();
	m_tauM   = new std::vector<double>();
	m_tauR   = new std::vector<double>();
	m_tauE_NPion = new std::vector<double>();
	m_tauJetEta = new std::vector<double>();
	m_tauJetPhi = new std::vector<double>();
	m_tauJetPt  = new std::vector<double>();
	m_tauJet_seedPt  = new std::vector<double>();
	m_tauJetE   = new std::vector<double>();
	m_tauJetM   = new std::vector<double>();
	m_tauJetR   = new std::vector<double>();
	m_MatchedEMPFjetEta = new std::vector<double>();
	m_MatchedEMPFjetPhi = new std::vector<double>();
	m_MatchedEMPFjetPt  = new std::vector<double>();
	m_MatchedEMPFjetE   = new std::vector<double>();
	m_MatchedEMPFjetM   = new std::vector<double>();
	m_MatchedEMPFjetR   = new std::vector<double>();
	m_MatchedLCTopojetEta = new std::vector<double>();
	m_MatchedLCTopojetPhi = new std::vector<double>();
	m_MatchedLCTopojetPt  = new std::vector<double>();
	m_MatchedLCTopojetE   = new std::vector<double>();
	m_MatchedLCTopojetM   = new std::vector<double>();
	m_MatchedLCTopojetR   = new std::vector<double>();
	m_MatchedEMPFtauEta = new std::vector<double>();
	m_MatchedEMPFtauPhi = new std::vector<double>();
	m_MatchedEMPFtauPt  = new std::vector<double>();
	m_MatchedEMPFtauE   = new std::vector<double>();
	m_MatchedEMPFtauE_NPion = new std::vector<double>();
	m_MatchedEMPFtauM   = new std::vector<double>();
	m_MatchedEMPFtauR   = new std::vector<double>();
	m_MatchedLCTopotauEta = new std::vector<double>();
	m_MatchedLCTopotauPhi = new std::vector<double>();
	m_MatchedLCTopotauPt  = new std::vector<double>();
	m_MatchedLCTopotauE   = new std::vector<double>();
	m_MatchedLCTopotauE_NPion = new std::vector<double>();
	m_MatchedLCTopotauM   = new std::vector<double>();
	m_MatchedLCTopotauR   = new std::vector<double>();
	
    m_EMPFChargedTrack = new std::vector<size_t>();
	m_EMPFChargedPFO = new std::vector<size_t>();
	m_EMPFNeutralPFO = new std::vector<size_t>();
	m_LCTopoChargedTrack = new std::vector<size_t>();
	m_LCTopoChargedPFO = new std::vector<size_t>();
	m_LCTopoNeutralPFO = new std::vector<size_t>();
	
	m_ChargedPion = new std::vector<size_t>();
	m_NeutralPion = new std::vector<size_t>();
	m_EMPFChargedPion = new std::vector<size_t>();
	m_EMPFNeutralPion = new std::vector<size_t>();
	m_LCTopoChargedPion = new std::vector<size_t>();
	m_LCTopoNeutralPion = new std::vector<size_t>();
	
	mytree->Branch ("RunNumber", &m_runNumber);
	mytree->Branch ("EventNumber", &m_eventNumber);
	mytree->Branch ("nMatchedEMPFJetNumber", &m_nEMPF);
	mytree->Branch ("nMatchedLCTopoJetNumber", &m_nLCTopo);
	mytree->Branch ("EMPFjets_eta", &m_EMPFjetEta);
	mytree->Branch ("EMPFjets_phi", &m_EMPFjetPhi);
	mytree->Branch ("EMPFjets_pt" , &m_EMPFjetPt );
	mytree->Branch ("EMPFjets_e"  , &m_EMPFjetE  );
	mytree->Branch ("EMPFjets_m"  , &m_EMPFjetM  );
	mytree->Branch ("EMPFjets_r"  , &m_EMPFjetR  );
	mytree->Branch ("LCTopojets_eta", &m_LCTopojetEta);
	mytree->Branch ("LCTopojets_phi", &m_LCTopojetPhi);
	mytree->Branch ("LCTopojets_pt" , &m_LCTopojetPt );
	mytree->Branch ("LCTopojets_e"  , &m_LCTopojetE  );
	mytree->Branch ("LCTopojets_m"  , &m_LCTopojetM  );
	mytree->Branch ("LCTopojets_r"  , &m_LCTopojetR  );
	mytree->Branch ("taus_number", &m_tauNum);
	mytree->Branch ("taus_eta", &m_tauEta);
	mytree->Branch ("taus_phi", &m_tauPhi);
	mytree->Branch ("taus_pt" , &m_tauPt );
	mytree->Branch ("taus_e"  , &m_tauE  );
	mytree->Branch ("taus_e_np"  , &m_tauE_NPion  );
	mytree->Branch ("taus_m"  , &m_tauM  );
	mytree->Branch ("taus_nChargedPion"  , &m_ChargedPion );//
	mytree->Branch ("taus_nNeutralPion"  , &m_NeutralPion );//
	//mytree->Branch ("taus_r"  , &m_tauR  );
	mytree->Branch ("taujets_eta", &m_tauJetEta);
	mytree->Branch ("taujets_phi", &m_tauJetPhi);
	mytree->Branch ("taujets_pt" , &m_tauJetPt );
	mytree->Branch ("taujets_seed_pt" , &m_tauJet_seedPt );
	mytree->Branch ("taujets_e"  , &m_tauJetE  );
	mytree->Branch ("taujets_m"  , &m_tauJetM  );
	mytree->Branch ("taujets_r"  , &m_tauJetR  );
	mytree->Branch ("MatchedEMPFjets_eta", &m_MatchedEMPFjetEta);
	mytree->Branch ("MatchedEMPFjets_phi", &m_MatchedEMPFjetPhi);
	mytree->Branch ("MatchedEMPFjets_pt" , &m_MatchedEMPFjetPt );
	mytree->Branch ("MatchedEMPFjets_e"  , &m_MatchedEMPFjetE  );
	mytree->Branch ("MatchedEMPFjets_m"  , &m_MatchedEMPFjetM  );
	mytree->Branch ("MatchedEMPFjets_r"  , &m_MatchedEMPFjetR  );
	mytree->Branch ("MatchedEMPFjets_nChargedTrack" , &m_EMPFChargedTrack);//
	mytree->Branch ("MatchedEMPFjets_nChargedPFO" , &m_EMPFChargedPFO);//
	mytree->Branch ("MatchedEMPFjets_nNeutralPFO" , &m_EMPFNeutralPFO);//
	mytree->Branch ("MatchedLCTopojets_eta", &m_MatchedLCTopojetEta);
	mytree->Branch ("MatchedLCTopojets_phi", &m_MatchedLCTopojetPhi);
	mytree->Branch ("MatchedLCTopojets_pt" , &m_MatchedLCTopojetPt );
	mytree->Branch ("MatchedLCTopojets_e"  , &m_MatchedLCTopojetE  );
	mytree->Branch ("MatchedLCTopojets_m"  , &m_MatchedLCTopojetM  );
	mytree->Branch ("MatchedLCTopojets_r"  , &m_MatchedLCTopojetR  );
	mytree->Branch ("MatchedLCTopojets_nChargedTrack" , &m_LCTopoChargedTrack);//
	mytree->Branch ("MatchedLCTopojets_nChargedPFO" , &m_LCTopoChargedPFO);//
	mytree->Branch ("MatchedLCTopojets_nNeutralPFO" , &m_LCTopoNeutralPFO);//
	
	mytree->Branch ("MatchedEMPFtaus_eta", &m_MatchedEMPFtauEta);
	mytree->Branch ("MatchedEMPFtaus_phi", &m_MatchedEMPFtauPhi);
	mytree->Branch ("MatchedEMPFtaus_pt" , &m_MatchedEMPFtauPt );
	mytree->Branch ("MatchedEMPFtaus_e"  , &m_MatchedEMPFtauE  );
	mytree->Branch ("MatchedEMPFtaus_e_np"  , &m_MatchedEMPFtauE_NPion  );
	mytree->Branch ("MatchedEMPFtaus_m"  , &m_MatchedEMPFtauM  );
	mytree->Branch ("MatchedEMPFtaus_nChargedPion"  , &m_EMPFChargedPion );
	mytree->Branch ("MatchedEMPFtaus_nNeutralPion"  , &m_EMPFNeutralPion );
	//mytree->Branch ("MatchedEMPFtaus_r"  , &m_MatchedEMPFtauR  );
	mytree->Branch ("MatchedLCTopotaus_eta", &m_MatchedLCTopotauEta);
	mytree->Branch ("MatchedLCTopotaus_phi", &m_MatchedLCTopotauPhi);
	mytree->Branch ("MatchedLCTopotaus_pt" , &m_MatchedLCTopotauPt );
	mytree->Branch ("MatchedLCTopotaus_e"  , &m_MatchedLCTopotauE  );
	mytree->Branch ("MatchedLCTopotaus_e_np"  , &m_MatchedLCTopotauE_NPion );
	mytree->Branch ("MatchedLCTopotaus_m"  , &m_MatchedLCTopotauM  );
	mytree->Branch ("MatchedLCTopotaus_nChargedPion"  , &m_LCTopoChargedPion );
	mytree->Branch ("MatchedLCTopotaus_nNeutralPion"  , &m_LCTopoNeutralPion );
	//mytree->Branch ("MatchedLCTopotaus_r"  , &m_MatchedLCTopotauR  );
	
	return StatusCode::SUCCESS;
}

StatusCode TruthAna::execute()
{

	// retrieve the eventInfo object from the event store
	//const xAOD::EventInfo* eventInfo = 0;
	//ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
	//m_runNumber = eventInfo->runNumber();
	//m_eventNumber = eventInfo->eventNumber();
	m_runNumber = 0;
	m_eventNumber = 0;
	//for( const xAOD::EventInfo::SubEvent& subEv : eventInfo->subEvents() ){
	//}
	
	// check if the event is data or MC
	//bool isMC = false;
	//if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ){
	//	isMC = true;
	//}
	//if (!isMC){
	//	if (!m_grl->passRunLB(*eventInfo) ){
	//		ANA_MSG_INFO ("drop event: GRL");
	//		return StatusCode::SUCCESS; // go to next event
	//	}
	//}
	// write branch
	// print out run and event number from retrieved object
	//ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
	//ANA_MSG_INFO ("retrive truth tau");
	//****************************************************************************
	//*** Attention! If you loop over the Truth particles / vertices directly, ***
	//*** you will get signal and pile-up together. So we use the Truth event. ***
	//****************************************************************************
	m_tauNum->clear();
	m_tauEta->clear();
	m_tauPhi->clear();
	m_tauPt->clear();
	m_tauE->clear();
	m_tauE_NPion->clear();
	m_tauM->clear();
	m_tauR->clear();
	m_ChargedPion->clear();
	m_NeutralPion->clear();
	const xAOD::TruthEventContainer* xTruthEvtContainer = NULL;
	std::vector<const xAOD::IParticle*> xTruthTauContainer;
	//const xAOD::TruthParticleContainer* xTruthPContainer = NULL;
	ANA_CHECK (evtStore()->retrieve (xTruthEvtContainer, "TruthEvents"));
	for (auto truthEvt : *xTruthEvtContainer) {
		const std::vector<float> weights = truthEvt->weights();
		//float scale = truthEvt->eventScale();
		for (const auto& truthPLink : truthEvt->truthParticleLinks() ){
			if (!truthPLink.isValid()) continue;
			const xAOD::TruthParticle* truthP = *truthPLink;
			//static SG::AuxElement::ConstAccessor<float> m_accPtVis("pt_vis");
			//if(m_accPtVis.isAvailable(*truthP)){ANA_MSG_INFO ("available");}
			//else{ANA_MSG_INFO ("no available");}
			//xAOD::TruthParticle truthTau = *truthP;
			//xAOD::TruthParticle **truthTauP = &truthP;
			//truthTau = truthP;
			//xAOD::TruthParticle* truthTau = truthP;
			if (!truthP->isTau()) continue;
			Utils::RecoTauVisDecay( *truthP ); 
			if ( truthP->auxdata<int>("IsTauDecay") < 1 ) continue;
			if ( truthP->auxdata<int>("IsHadronicTau") < 1 ) continue;
			if ( Utils::PassCaloShape(truthP->auxdata<double>("eta_vis")) ) 
			{
				m_tauEta->push_back( truthP->auxdata<double>("eta_vis") );
				m_tauPhi->push_back( truthP->auxdata<double>("phi_vis") );
				m_tauPt ->push_back( truthP->auxdata<double>("pt_vis") );
				m_tauM  ->push_back( truthP->auxdata<double>("m_vis") );
				m_tauE  ->push_back( truthP->auxdata<double>("e_vis") );
				m_tauE_NPion  ->push_back( truthP->auxdata<double>("e_vis_neutral_pions") );
				m_tauR  ->push_back( truthP->rapidity() );
				m_ChargedPion->push_back(truthP->auxdata<size_t>("numChargedPion"));
				m_NeutralPion->push_back(truthP->auxdata<size_t>("numNeutralPion"));
				hist("HadTau_DecayMode")->Fill( (truthP->auxdata<size_t>("numChargedPion")-1)*5 + truthP->auxdata<size_t>("numNeutralPion") );
				hist("Truth_tauPt")->Fill( truthP->auxdata<double>("pt_vis") * 0.001 );
				xTruthTauContainer.push_back(truthP);
			}
		}
	}
	m_tauNum->push_back(m_tauEta->size());

	//ANA_MSG_INFO ("retrive taujet");
	// Taujet cont
	const xAOD::TauJetContainer* taus = nullptr;
	std::vector<const xAOD::IParticle*> vTauJet;
	ANA_CHECK (evtStore()->retrieve (taus, "TauJets"));
	m_tauJetEta ->clear();
	m_tauJetPhi ->clear();
	m_tauJetPt  ->clear();
	m_tauJet_seedPt ->clear();
	m_tauJetE   ->clear();
	m_tauJetM   ->clear();
	m_tauJetR   ->clear();
	for (auto taujet : *taus) {
		if ( taujet->ptJetSeed() >= 0 ) {
			m_tauJetEta->push_back( taujet->eta() );
			m_tauJetPhi->push_back( taujet->phi() );
			m_tauJetPt->push_back(  taujet->pt() );
			m_tauJet_seedPt->push_back(  taujet->ptJetSeed() );
			m_tauJetE->push_back(   taujet->e() );
			m_tauJetM->push_back(   taujet->m() );
			m_tauJetR->push_back(   taujet->rapidity() );
			vTauJet.push_back( taujet );
		}
	}
	
	// loop over the jets in the container
	// EMPFlow jets
	const xAOD::JetContainer* EMPFjets = nullptr;
	ANA_CHECK (evtStore()->retrieve (EMPFjets, "AntiKt4EMPFlowJets"));
	m_EMPFjetEta->clear();
	m_EMPFjetPhi->clear();
	m_EMPFjetPt->clear();
	m_EMPFjetE->clear();
	m_EMPFjetM->clear();
	m_EMPFjetR->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *EMPFjets) {
		hist("EMPF_jetPt")->Fill(jet->pt()*0.001);
		m_EMPFjetEta->push_back( jet->eta() );
		m_EMPFjetPhi->push_back( jet->phi() );
		m_EMPFjetPt->push_back( jet->pt() );
		m_EMPFjetE->push_back( jet->e() );
		m_EMPFjetM->push_back( jet->m() );
		m_EMPFjetR->push_back( jet->rapidity() );
	}
	// LCtopo jets cont
	const xAOD::JetContainer* LCTopojets = nullptr;
	ANA_CHECK (evtStore()->retrieve (LCTopojets, "AntiKt4LCTopoJets"));
	m_LCTopojetEta->clear();
	m_LCTopojetPhi->clear();
	m_LCTopojetPt->clear();
	m_LCTopojetE->clear();
	m_LCTopojetM->clear();
	m_LCTopojetR->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *LCTopojets) {
		hist("LCTopo_jetPt")->Fill(jet->pt()*0.001);
		m_LCTopojetEta->push_back( jet->eta() );
		m_LCTopojetPhi->push_back( jet->phi() );
		m_LCTopojetPt->push_back( jet->pt() );
		m_LCTopojetE->push_back( jet->e() );
		m_LCTopojetM->push_back( jet->m() );
		m_LCTopojetR->push_back( jet->rapidity() );
	}
	// Fill the event into the tree:
	
	
	//tau jet match test
	auto EMPFmatch = Utils::findMatchDRI(vTauJet,xTruthTauContainer,0.2);
	m_MatchedEMPFjetEta->clear();
	m_MatchedEMPFjetPhi->clear();
	m_MatchedEMPFjetPt->clear();
	m_MatchedEMPFjetE->clear();
	m_MatchedEMPFjetM->clear();
	m_MatchedEMPFjetR->clear();
	m_EMPFChargedTrack->clear();
	m_EMPFChargedPFO->clear();
	m_EMPFNeutralPFO->clear();
	m_MatchedEMPFtauEta->clear();
	m_MatchedEMPFtauPhi->clear();
	m_MatchedEMPFtauPt->clear();
	m_MatchedEMPFtauE->clear();
	m_MatchedEMPFtauE_NPion->clear();
	m_MatchedEMPFtauM->clear();
	m_MatchedEMPFtauR->clear();
	m_EMPFChargedPion->clear();
	m_EMPFNeutralPion->clear();
	m_nEMPF = EMPFmatch.size();
	for ( auto taujetsjet : EMPFmatch ) {
		if ( Utils::PassCaloShape(taujetsjet.second.second->eta()) && Utils::PassCaloShape(taujetsjet.second.first->auxdata<double>("eta_vis")) ) 
		{
			hist("MatchedEMPF_jetPt")->Fill( taujetsjet.second.second->pt() * 0.001 );
			m_MatchedEMPFjetEta->push_back( taujetsjet.second.second->eta() );
			m_MatchedEMPFjetPhi->push_back( taujetsjet.second.second->phi() );
			m_MatchedEMPFjetPt ->push_back( taujetsjet.second.second->pt() );
			m_MatchedEMPFjetE -> push_back( taujetsjet.second.second->e() );
			m_MatchedEMPFjetM -> push_back( taujetsjet.second.second->m() );
			m_MatchedEMPFjetR -> push_back( taujetsjet.second.second->rapidity() );
			m_EMPFChargedTrack-> push_back( ((xAOD::TauJet*)taujetsjet.second.second)->nTracksCharged() );
			m_EMPFChargedPFO-> push_back(   ((xAOD::TauJet*)taujetsjet.second.second)->nChargedPFOs() );
			m_EMPFNeutralPFO-> push_back(   ((xAOD::TauJet*)taujetsjet.second.second)->nPi0PFOs() );
			hist("MatchedEMPF_tauPt")->Fill( taujetsjet.second.first->auxdata<double>("pt_vis") * 0.001 );
			m_MatchedEMPFtauEta->push_back( taujetsjet.second.first->auxdata<double>("eta_vis") );
			m_MatchedEMPFtauPhi->push_back( taujetsjet.second.first->auxdata<double>("phi_vis") );
			m_MatchedEMPFtauPt ->push_back( taujetsjet.second.first->auxdata<double>("pt_vis") );
			m_MatchedEMPFtauM  ->push_back( taujetsjet.second.first->auxdata<double>("m_vis") );
			m_MatchedEMPFtauE  ->push_back( taujetsjet.second.first->auxdata<double>("e_vis") );
			m_MatchedEMPFtauE_NPion ->push_back( taujetsjet.second.first->auxdata<double>("e_vis_neutral_pions") );
			m_MatchedEMPFtauR  ->push_back( taujetsjet.second.first->rapidity() );
			m_EMPFChargedPion->push_back( taujetsjet.second.first->auxdata<size_t>("numChargedPion"));
			m_EMPFNeutralPion->push_back( taujetsjet.second.first->auxdata<size_t>("numNeutralPion"));
		}
	}
	
	auto LCTopomatch = Utils::findMatchDRI(vTauJet,xTruthTauContainer,0.2);
	m_MatchedLCTopojetEta->clear();
	m_MatchedLCTopojetPhi->clear();
	m_MatchedLCTopojetPt->clear();
	m_MatchedLCTopojetE->clear();
	m_MatchedLCTopojetM->clear();
	m_MatchedLCTopojetR->clear();
	m_LCTopoChargedTrack->clear();
	m_LCTopoChargedPFO->clear();
	m_LCTopoNeutralPFO->clear();
	m_MatchedLCTopotauEta->clear();
	m_MatchedLCTopotauPhi->clear();
	m_MatchedLCTopotauPt->clear();
	m_MatchedLCTopotauE->clear();
	m_MatchedLCTopotauE_NPion->clear();
	m_MatchedLCTopotauM->clear();
	m_MatchedLCTopotauR->clear();
	m_LCTopoChargedPion->clear();
	m_LCTopoNeutralPion->clear();
	m_nLCTopo = LCTopomatch.size();
	for ( auto taujetsjet : LCTopomatch ) {
		if ( Utils::PassCaloShape(taujetsjet.second.second->eta()) && Utils::PassCaloShape(taujetsjet.second.first->auxdata<double>("eta_vis")) ) 
		{
			hist("MatchedLCTopo_jetPt")->Fill( taujetsjet.second.second->pt() * 0.001 );
			m_MatchedLCTopojetEta->push_back( taujetsjet.second.second->eta() );
			m_MatchedLCTopojetPhi->push_back( taujetsjet.second.second->phi() );
			m_MatchedLCTopojetPt ->push_back( taujetsjet.second.second->pt() );
			m_MatchedLCTopojetE -> push_back( taujetsjet.second.second->e() );
			m_MatchedLCTopojetM -> push_back( taujetsjet.second.second->m() );
			m_MatchedLCTopojetR -> push_back( taujetsjet.second.second->rapidity() );
			m_LCTopoChargedTrack-> push_back( ((xAOD::TauJet*)taujetsjet.second.second)->nTracksCharged() );
			m_LCTopoChargedPFO-> push_back(   ((xAOD::TauJet*)taujetsjet.second.second)->nChargedPFOs() );
			m_LCTopoNeutralPFO-> push_back(   ((xAOD::TauJet*)taujetsjet.second.second)->nPi0PFOs() );
			hist("MatchedLCTopo_tauPt")->Fill( taujetsjet.second.first->auxdata<double>("pt_vis") * 0.001 );
			m_MatchedLCTopotauEta->push_back( taujetsjet.second.first->auxdata<double>("eta_vis") );
			m_MatchedLCTopotauPhi->push_back( taujetsjet.second.first->auxdata<double>("phi_vis") );
			m_MatchedLCTopotauPt ->push_back( taujetsjet.second.first->auxdata<double>("pt_vis") );
			m_MatchedLCTopotauM  ->push_back( taujetsjet.second.first->auxdata<double>("m_vis") );
			m_MatchedLCTopotauE  ->push_back( taujetsjet.second.first->auxdata<double>("e_vis") );
			m_MatchedLCTopotauE_NPion ->push_back( taujetsjet.second.first->auxdata<double>("e_vis_neutral_pions") );
			m_MatchedLCTopotauR  ->push_back( taujetsjet.second.first->rapidity() );
			m_LCTopoChargedPion->push_back( taujetsjet.second.first->auxdata<size_t>("numChargedPion"));
			m_LCTopoNeutralPion->push_back( taujetsjet.second.first->auxdata<size_t>("numNeutralPion"));
		}
	}
	//ANA_MSG_INFO ("n_jet="<<EMPFjets->size()<<":"<<LCTopojets->size()<<" n_tau="<<taus->size()<< " Match="<<nEMPF<<":"<<nLCTopo );

	// last!
	tree ("analysis")->Fill ();
	
	return StatusCode::SUCCESS;
}

StatusCode TruthAna::finalize()
{
	//write
	return StatusCode::SUCCESS;
}


TruthAna::~TruthAna()
{
	delete m_EMPFjetEta;
	delete m_EMPFjetPhi;
	delete m_EMPFjetPt ;
	delete m_EMPFjetE  ;
	delete m_EMPFjetM  ;
	delete m_EMPFjetR  ;
	delete m_LCTopojetEta;
	delete m_LCTopojetPhi;
	delete m_LCTopojetPt ;
	delete m_LCTopojetE  ;
	delete m_LCTopojetM  ;
	delete m_LCTopojetR  ;
	delete m_tauNum;
	delete m_tauEta;
	delete m_tauPhi;
	delete m_tauPt ;
	delete m_tauE  ;
	delete m_tauE_NPion  ;
	delete m_tauM  ;
	delete m_tauR  ;
	delete m_tauJetEta;
	delete m_tauJetPhi;
	delete m_tauJetPt ;
	delete m_tauJet_seedPt ;
	delete m_tauJetE  ;
	delete m_tauJetM  ;
	delete m_tauJetR  ;
	delete m_MatchedEMPFjetEta;
	delete m_MatchedEMPFjetPhi;
	delete m_MatchedEMPFjetPt ;
	delete m_MatchedEMPFjetE  ;
	delete m_MatchedEMPFjetM  ;
	delete m_MatchedEMPFjetR  ;
	delete m_EMPFChargedTrack ;
	delete m_EMPFChargedPFO ;
	delete m_EMPFNeutralPFO ;
	delete m_MatchedLCTopojetEta;
	delete m_MatchedLCTopojetPhi;
	delete m_MatchedLCTopojetPt ;
	delete m_MatchedLCTopojetE  ;
	delete m_MatchedLCTopojetM  ;
	delete m_MatchedLCTopojetR  ;
	delete m_LCTopoChargedTrack ;
	delete m_LCTopoChargedPFO ;
	delete m_LCTopoNeutralPFO ;
	delete m_MatchedEMPFtauEta;
	delete m_MatchedEMPFtauPhi;
	delete m_MatchedEMPFtauPt ;
	delete m_MatchedEMPFtauE  ;
	delete m_MatchedEMPFtauE_NPion ;
	delete m_MatchedEMPFtauM  ;
	delete m_MatchedEMPFtauR  ;
	delete m_MatchedLCTopotauEta;
	delete m_MatchedLCTopotauPhi;
	delete m_MatchedLCTopotauPt ;
	delete m_MatchedLCTopotauE  ;
	delete m_MatchedLCTopotauE_NPion ;
	delete m_MatchedLCTopotauM  ;
	delete m_MatchedLCTopotauR  ;
}
