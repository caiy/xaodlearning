#include "MyAnalysis/Utils.h"

bool Utils::PassCaloShape(double eta){
	if( fabs(eta) < 1.37 ){
		return true;
	}else if( fabs(eta) < 1.52 ){
		return false;
	}else if( fabs(eta) < 2.5 ){
		return true;
	}else{
		return false;
	}
}

bool Utils::RecoTauVisDecay(const xAOD::TruthParticle& xTruthParticle){
	
	const xAOD::TruthVertex* xDecayVertex = xTruthParticle.decayVtx();
	
	std::vector<int> xPdgID;
	int IsHad = 0;
	int IsDecay = 0;

	std::size_t iNChargedPions = 0;
	std::size_t iNNeutralPions = 0;
	std::size_t iNChargedOthers = 0;
	std::size_t iNNeutralOthers = 0;
	std::size_t iNChargedDaughters = 0;
	std::vector<int> vDecayMode;
	
	TLorentzVector vTruthVisTLV;
	TLorentzVector vTruthVisTLVCharged;
	TLorentzVector vTruthVisTLVChargedPions;
	TLorentzVector vTruthVisTLVChargedOthers;
	TLorentzVector vTruthVisTLVNeutral;
	TLorentzVector vTruthVisTLVNeutralPions;
	TLorentzVector vTruthVisTLVNeutralOthers;  
	
	static SG::AuxElement::Decorator<int> decIsTauDecay("IsTauDecay");
    static SG::AuxElement::Decorator<int> decIsHadronicTau("IsHadronicTau");

	//is Tau decay?
	if (!xDecayVertex){
		decIsTauDecay(xTruthParticle) = IsDecay;
	    //decIsHadronicTau(xTruthParticle) = IsHad;
		return true;
	}// if not decay return and set "IsDecay" =0

	IsDecay = 1;
	decIsTauDecay(xTruthParticle) = IsDecay;

	for ( size_t iOutgoingParticle = 0; iOutgoingParticle < xDecayVertex->nOutgoingParticles(); ++iOutgoingParticle )
	{
		const xAOD::TruthParticle* xTruthDaughter = xDecayVertex->outgoingParticle(iOutgoingParticle);
		
		if (!xTruthDaughter)
		{
			std::cout<<"you shall not pass"<<std::endl;
			//ANA_MSG_INFO("Truth daughter of tau decay was not found in container. Please ensure that this container has the full tau decay information or produce the TruthTaus container in AtlasDerivation.\nInformation on how to do this can be found here:\nhttps://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TauPreRecommendations2015#Accessing_Tau_Truth_Information");
	        decIsHadronicTau(xTruthParticle) = IsHad;
			return false;
		}//end MSG FATAL
		
		int iAbsPdgId = xTruthDaughter->absPdgId();
		int iPdgId = xTruthDaughter->pdgId();
		if (xTruthDaughter->status() == 2 or xTruthDaughter->status() == 11 or xTruthDaughter->status() == 10902) // 11 for HERWIG++
		{
			if ( iAbsPdgId != 111 and iAbsPdgId != 311 and iAbsPdgId != 310 and iAbsPdgId != 130 ) continue;
		}
		
		// only process stable particles and pions and kaons which passed above criteria
		if (xTruthDaughter->status() != 1 and xTruthDaughter->status() != 2 and xTruthDaughter->status() != 11 and xTruthDaughter->status() != 10902) continue;
		
		//record daughter pdgID
		xPdgID.push_back(iPdgId);
		
		// add pdgID to vector for decay mode classification
		vDecayMode.push_back(iPdgId);

		//tau to nu , skip
		if ( xTruthDaughter->isNeutrino() ) continue;
	
		//tau to tau , skip
		if ( xTruthDaughter->isTau() ) continue;
		
		//record is it hadronic-decay
		if ( xTruthDaughter->isHadron() ) IsHad = 1;
		
		
		// add momentum of non-neutrino particle to visible momentum
		vTruthVisTLV += xTruthDaughter->p4();
		
		// count charged decay particles
		if ( xTruthDaughter->isCharged() )
		{
			vTruthVisTLVCharged += xTruthDaughter->p4();
			iNChargedDaughters++;
			if (iAbsPdgId==211) {
				vTruthVisTLVChargedPions += xTruthDaughter->p4();
				iNChargedPions++;
			} 
			else 
			{
				vTruthVisTLVChargedOthers += xTruthDaughter->p4();
				iNChargedOthers++;
			}
		}
		
		// count neutral pions
		if ( xTruthDaughter->isNeutral() )
		{
			if (iAbsPdgId==111)
			{
				vTruthVisTLVNeutralPions += xTruthDaughter->p4();
				iNNeutralPions++;
			}
			else
			{
				vTruthVisTLVNeutralOthers += xTruthDaughter->p4();
				iNNeutralOthers++;
			}
		}
	}//end of daughter particle
	//now we need to write visible Info and Had Info to &

	
	// Invisible part
	TLorentzVector vTruthInvisTLV = xTruthParticle.p4() - vTruthVisTLV;
	static SG::AuxElement::Decorator<double> decPtInvis("pt_invis");
	static SG::AuxElement::Decorator<double> decEtaInvis("eta_invis");
	static SG::AuxElement::Decorator<double> decPhiInvis("phi_invis");
	static SG::AuxElement::Decorator<double> decMInvis("m_invis");
	static SG::AuxElement::Decorator<double> decEInvis("e_invis");
	decPtInvis(xTruthParticle)  = vTruthInvisTLV.Pt();
	decEtaInvis(xTruthParticle) = vTruthInvisTLV.Eta();
	decPhiInvis(xTruthParticle) = vTruthInvisTLV.Phi();
	decMInvis(xTruthParticle)   = vTruthInvisTLV.M();
	decEInvis(xTruthParticle)   = vTruthInvisTLV.E();


	// Visible part
	// Charged particles
    static SG::AuxElement::Decorator<double> decPtVisCharged("pt_vis_charged");
    static SG::AuxElement::Decorator<double> decEtaVisCharged("eta_vis_charged");
    static SG::AuxElement::Decorator<double> decPhiVisCharged("phi_vis_charged");
    static SG::AuxElement::Decorator<double> decMVisCharged("m_vis_charged");
    static SG::AuxElement::Decorator<double> decEVisCharged("e_vis_charged");
    decPtVisCharged(xTruthParticle)  = vTruthVisTLVCharged.Pt();
    decEtaVisCharged(xTruthParticle) = vTruthVisTLVCharged.Eta();
    decPhiVisCharged(xTruthParticle) = vTruthVisTLVCharged.Phi();
    decMVisCharged(xTruthParticle)   = vTruthVisTLVCharged.M();
    decEVisCharged(xTruthParticle)   = vTruthVisTLVCharged.E();
	// Charged Pions
    static SG::AuxElement::Decorator<double> decPtVisChargedPions("pt_vis_charged_pions");
    static SG::AuxElement::Decorator<double> decEtaVisChargePion("eta_vis_charged_pions");
    static SG::AuxElement::Decorator<double> decPhiVisChargedPions("phi_vis_charged_pions");
    static SG::AuxElement::Decorator<double> decMVisChargedPions("m_vis_charged_pions");
    static SG::AuxElement::Decorator<double> decEVisChargedPions("e_vis_charged_pions");
    decPtVisChargedPions(xTruthParticle)  = vTruthVisTLVChargedPions.Pt();
    decEtaVisChargePion(xTruthParticle)   = vTruthVisTLVChargedPions.Eta();
    decPhiVisChargedPions(xTruthParticle) = vTruthVisTLVChargedPions.Phi();
    decMVisChargedPions(xTruthParticle)   = vTruthVisTLVChargedPions.M();
    decEVisChargedPions(xTruthParticle)   = vTruthVisTLVChargedPions.E();
	// Charged others
    static SG::AuxElement::Decorator<double> decPtVisChargedOthers("pt_vis_charged_others");
    static SG::AuxElement::Decorator<double> decEtaVisChargedOthers("eta_vis_charged_others");
    static SG::AuxElement::Decorator<double> decPhiVisChargedOthers("phi_vis_charged_others");
    static SG::AuxElement::Decorator<double> decMVisChargedOthers("m_vis_charged_others");
    static SG::AuxElement::Decorator<double> decEVisChargedOthers("e_vis_charged_others");
    decPtVisChargedOthers(xTruthParticle)  = vTruthVisTLVChargedOthers.Pt();
    decEtaVisChargedOthers(xTruthParticle) = vTruthVisTLVChargedOthers.Eta();
    decPhiVisChargedOthers(xTruthParticle) = vTruthVisTLVChargedOthers.Phi();
    decMVisChargedOthers(xTruthParticle)   = vTruthVisTLVChargedOthers.M();
    decEVisChargedOthers(xTruthParticle)   = vTruthVisTLVChargedOthers.E();
	// Neutral particles
    static SG::AuxElement::Decorator<double> decPtVisNeutral("pt_vis_neutral");
    static SG::AuxElement::Decorator<double> decEtaVisNeutral("eta_vis_neutral");
    static SG::AuxElement::Decorator<double> decPhiVisNeutral("phi_vis_neutral");
    static SG::AuxElement::Decorator<double> decMVisNeutral("m_vis_neutral");
    static SG::AuxElement::Decorator<double> decEVisNeutral("e_vis_neutral");
    decPtVisNeutral(xTruthParticle)  = vTruthVisTLVNeutral.Pt();
    decEtaVisNeutral(xTruthParticle) = vTruthVisTLVNeutral.Eta();
    decPhiVisNeutral(xTruthParticle) = vTruthVisTLVNeutral.Phi();
    decMVisNeutral(xTruthParticle)   = vTruthVisTLVNeutral.M();
    decEVisNeutral(xTruthParticle)   = vTruthVisTLVNeutral.E();
	// Neutral Pions
    static SG::AuxElement::Decorator<double> decPtVisNeutralPions("pt_vis_neutral_pions");
    static SG::AuxElement::Decorator<double> decEtaVisNeutralPions("eta_vis_neutral_pions");
    static SG::AuxElement::Decorator<double> decPhiVisNeutralPions("phi_vis_neutral_pions");
    static SG::AuxElement::Decorator<double> decMVisNeutralPions("m_vis_neutral_pions");
    static SG::AuxElement::Decorator<double> decEVisNeutralPions("e_vis_neutral_pions");
    decPtVisNeutralPions(xTruthParticle)  = vTruthVisTLVNeutralPions.Pt();
    decEtaVisNeutralPions(xTruthParticle) = vTruthVisTLVNeutralPions.Eta();
    decPhiVisNeutralPions(xTruthParticle) = vTruthVisTLVNeutralPions.Phi();
    decMVisNeutralPions(xTruthParticle)   = vTruthVisTLVNeutralPions.M();
    decEVisNeutralPions(xTruthParticle)   = vTruthVisTLVNeutralPions.E();
	// Neutral others
    static SG::AuxElement::Decorator<double> decPtVisNeutralOthers("pt_vis_neutral_others");
    static SG::AuxElement::Decorator<double> decEtaVisNeutralOthers("eta_vis_neutral_others");
    static SG::AuxElement::Decorator<double> decPhiVisNeutralOthers("phi_vis_neutral_others");
    static SG::AuxElement::Decorator<double> decMVisNeutralOthers("m_vis_neutral_others");
    static SG::AuxElement::Decorator<double> decEVisNeutralOthers("e_vis_neutral_others");
    decPtVisNeutralOthers(xTruthParticle)  = vTruthVisTLVNeutralOthers.Pt();
    decEtaVisNeutralOthers(xTruthParticle) = vTruthVisTLVNeutralOthers.Eta();
    decPhiVisNeutralOthers(xTruthParticle) = vTruthVisTLVNeutralOthers.Phi();
    decMVisNeutralOthers(xTruthParticle)   = vTruthVisTLVNeutralOthers.M();
    decEVisNeutralOthers(xTruthParticle)   = vTruthVisTLVNeutralOthers.E();
	
	
	// decay mode Vector
	static SG::AuxElement::Decorator<std::vector<int> > decDecayModeVector("DecayModeVector");
	decDecayModeVector(xTruthParticle) = vDecayMode;
	
	// Number of daughter particle
	static SG::AuxElement::Decorator<std::size_t> decNumCharged("numCharged");
	static SG::AuxElement::Decorator<std::size_t> decNumChargedPion("numChargedPion");
	static SG::AuxElement::Decorator<std::size_t> decNumNeutral("numNeutral");
	static SG::AuxElement::Decorator<std::size_t> decNumNeutralPion("numNeutralPion");
	decNumCharged(xTruthParticle) = iNChargedDaughters;
	decNumChargedPion(xTruthParticle) = iNChargedPions;
	decNumNeutral(xTruthParticle) = iNNeutralPions+iNNeutralOthers;
	decNumNeutralPion(xTruthParticle) = iNNeutralPions;
	
	// visible Tau 4-momentum
	static SG::AuxElement::Decorator<double> decPtVis("pt_vis");
	static SG::AuxElement::Decorator<double> decEtaVis("eta_vis");
	static SG::AuxElement::Decorator<double> decPhiVis("phi_vis");
	static SG::AuxElement::Decorator<double> decMVis("m_vis");
	static SG::AuxElement::Decorator<double> decEVis("e_vis");
	decPtVis(xTruthParticle)  = vTruthVisTLV.Pt();
	decEtaVis(xTruthParticle) = vTruthVisTLV.Eta();
	decPhiVis(xTruthParticle) = vTruthVisTLV.Phi();
	decMVis(xTruthParticle)   = vTruthVisTLV.M();
	decEVis(xTruthParticle)   = vTruthVisTLV.E();
	
	decIsHadronicTau(xTruthParticle) = IsHad;
	
	return true;
}


TLorentzVector Utils::GetVisP4(const xAOD::TruthParticle* tauCont){
	TLorentzVector vTruthVisTLV;
	static SG::AuxElement::ConstAccessor<double> m_accPtVis("pt_vis");
	static SG::AuxElement::ConstAccessor<double> m_accEtaVis("eta_vis");
	static SG::AuxElement::ConstAccessor<double> m_accPhiVis("phi_vis");
	static SG::AuxElement::ConstAccessor<double> m_accMVis("m_vis");
	vTruthVisTLV.SetPtEtaPhiM(
			m_accPtVis(*tauCont),
			m_accEtaVis(*tauCont),
			m_accPhiVis(*tauCont),
			m_accMVis(*tauCont) );
	return vTruthVisTLV;
}


TLorentzVector Utils::GetInvisP4(const xAOD::TruthParticle* tauCont){
	TLorentzVector vTruthVisTLV;
	static SG::AuxElement::ConstAccessor<double> m_accPtVis("pt_invis");
	static SG::AuxElement::ConstAccessor<double> m_accEtaVis("eta_invis");
	static SG::AuxElement::ConstAccessor<double> m_accPhiVis("phi_invis");
	static SG::AuxElement::ConstAccessor<double> m_accMVis("m_invis");
	vTruthVisTLV.SetPtEtaPhiM(
			m_accPtVis(*tauCont),
			m_accEtaVis(*tauCont),
			m_accPhiVis(*tauCont),
			m_accMVis(*tauCont) );
	TLorentzVector vTruthInvisTLV = tauCont->p4() - vTruthVisTLV;
	return vTruthInvisTLV;
}


std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > Utils::findMatchDR(const xAOD::JetContainer* jetCont , 
		std::vector<const xAOD::TruthParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::Jet* best_jet = nullptr;
	for (const auto jet : *jetCont) {
		++ijet;

		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			//double DPhi = fabs( tau->phi() - jet->phi() ); 
			//while (DPhi > M_PI) DPhi = fabs(2*M_PI - DPhi);
			//currentDR = sqrt( pow(( tau->eta() - jet->eta() )  ,2) + pow(DPhi ,2) );
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop

		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Jet*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			//double DPhi = fabs( match_map[best_itau].first->phi() - match_map[best_itau].second->phi() ); 
			//while (DPhi > M_PI) DPhi = fabs(2*M_PI - DPhi);
			//float old_DR = sqrt( pow( ( match_map[best_itau].first->eta() - match_map[best_itau].second->eta() )  ,2) + pow(DPhi ,2) );
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Jet* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}

std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > Utils::findMatchDR(const xAOD::JetContainer* jetCont , 
		const xAOD::TruthParticleContainer* tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::Jet* best_jet = nullptr;
	for (const auto jet : *jetCont) {
		++ijet;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : *tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);
			//double DPhi = fabs( tau->phi() - jet->phi() ); 
			//while (DPhi > M_PI) DPhi = fabs(2*M_PI - DPhi);
			//currentDR = sqrt( pow(( tau->eta() - jet->eta() )  ,2) + pow(DPhi ,2) );

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
	
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Jet*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			//double DPhi = fabs( match_map[best_itau].first->phi() - match_map[best_itau].second->phi() ); 
			//while (DPhi > M_PI) DPhi = fabs(2*M_PI - DPhi);
			//float old_DR = sqrt( pow( ( match_map[best_itau].first->eta() - match_map[best_itau].second->eta() )  ,2) + pow(DPhi ,2) );
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Jet* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > Utils::findMatchDR(const xAOD::TauJetContainer* jetCont , 
		std::vector<const xAOD::TruthParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::TauJet* best_jet = nullptr;

	for (const auto jet : *jetCont) {
		++ijet;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::TauJet*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > Utils::findMatchDR(const xAOD::TauJetContainer* jetCont , 
		const xAOD::TruthParticleContainer* tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::TauJet* best_jet = nullptr;

	for (const auto jet : *jetCont) {
		++ijet;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : *tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::TauJet*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


//
std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > Utils::findMatchDR( std::vector<const xAOD::TauJet*> jetCont , 
		std::vector<const xAOD::TruthParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::TauJet* best_jet = nullptr;

	for (const auto jet : jetCont) {
		++ijet;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::TauJet*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Electron* > > Utils::findMatchDR( std::vector<const xAOD::Electron*> eleCont , 
		std::vector<const xAOD::TruthParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Electron* > > match_map;
	int iele = -1;
	int best_iele = -1;
	const xAOD::Electron* best_ele = nullptr;

	for (const auto ele : eleCont) {
		++iele;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = ele->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_ele = ele;
				best_itau = itau;
				best_iele = iele;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Electron* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_iele != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Electron*>(best_tau, best_ele);
		}else if (got != match_map.end() and best_itau != -1 and best_iele != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Electron* >(best_tau, best_ele);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > Utils::findMatchDR( std::vector<const xAOD::Jet*> jetCont , 
		std::vector<const xAOD::TruthParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::Jet* best_jet = nullptr;

	for (const auto jet : jetCont) {
		++ijet;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Jet*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::Jet* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


std::unordered_map<int, std::pair<const xAOD::IParticle*, const xAOD::IParticle* > > Utils::findMatchDRI( std::vector<const xAOD::IParticle*> jetCont , 
		std::vector<const xAOD::IParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::IParticle*, const xAOD::IParticle* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::IParticle* best_jet = nullptr;

	for (const auto jet : jetCont) {
		++ijet;
		
		const xAOD::IParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			auto tauVisP4 = tau->p4();
			//auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::IParticle*, const xAOD::IParticle* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::IParticle*, const xAOD::IParticle*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = match_map[best_itau].first -> p4();
			//auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::IParticle*, const xAOD::IParticle* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}


std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::IParticle* > > Utils::findMatchDRI( std::vector<const xAOD::IParticle*> jetCont , 
		std::vector<const xAOD::TruthParticle*> tauCont , float bestDR = 0.2 ){

	std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::IParticle* > > match_map;
	int ijet = -1;
	int best_ijet = -1;
	const xAOD::IParticle* best_jet = nullptr;

	for (const auto jet : jetCont) {
		++ijet;
		
		const xAOD::TruthParticle* best_tau = nullptr;
		float currentDR = 0;
		float best_DR = bestDR;
		int itau = -1;
		int best_itau = -1;

		for (const auto tau : tauCont) {
			++itau;
			auto tauVisP4 = GetVisP4(tau);
			currentDR = jet->p4().DeltaR(tauVisP4);

			if (currentDR < best_DR){
				best_DR = currentDR;
				best_tau = tau;
				best_jet = jet;
				best_itau = itau;
				best_ijet = ijet;
			}
		}//end of Tau Loop
		
		//if a new match is found for a previous 
		//tau keep the new match if it is better 
		std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::IParticle* > >::const_iterator got = match_map.find (best_itau);
		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
			match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::IParticle*>(best_tau, best_jet);
		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
			auto tauVisP4 = GetVisP4(match_map[best_itau].first);
			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
			if (old_DR > best_DR){
				match_map[best_itau] = std::pair<const xAOD::TruthParticle*, const xAOD::IParticle* >(best_tau, best_jet);
			}
		}//endl review

	}//end of Jet Loop
	return match_map;
}

//std::unordered_map<int, std::pair<const std::unique_ptr<xAOD::IParticle>, const std::unique_ptr<xAOD::IParticle> > > Utils::findMatchDRI( std::vector< const xAOD::IParticle* >jetCont , 
//		std::vector< const xAOD::IParticle* > tauCont , float bestDR = 0.2 ){
//
//	std::unordered_map<int, std::pair<const std::unique_ptr<xAOD::IParticle>, const std::unique_ptr<xAOD::IParticle> > > match_map;
//	int ijet = -1;
//	int best_ijet = -1;
//	const std::unique_ptr<xAOD::IParticle> best_jet ;
//
//	for (const auto jet : jetCont) {
//		++ijet;
//	    
//		const std::unique_ptr<xAOD::IParticle> best_tau ;
//		
//		float currentDR = 0;
//		float best_DR = bestDR;
//		int itau = -1;
//		int best_itau = -1;
//
//		for (const auto tau : tauCont) {
//			++itau;
//			auto tauVisP4 = tau->p4();
//			currentDR = jet->p4().DeltaR(tauVisP4);
//
//			if (currentDR < best_DR){
//				best_DR = currentDR;
//                auto Ctau = std::make_unique<xAOD::IParticle>(tau);
//                auto Cjet = std::make_unique<xAOD::IParticle>(jet);
//				best_tau = std::move( Ctau );
//				best_jet = std::move( Cjet );
//				best_itau = itau;
//				best_ijet = ijet;
//			}
//		}//end of Tau Loop
//		
//		//if a new match is found for a previous 
//		//tau keep the new match if it is better 
//		std::unordered_map<int, std::pair<const std::unique_ptr<xAOD::IParticle>, const std::unique_ptr<xAOD::IParticle> > >::const_iterator got = match_map.find (best_itau);
//		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
//			match_map[best_itau] = std::pair<const std::unique_ptr<xAOD::IParticle>, const std::unique_ptr<xAOD::IParticle> >(best_tau, best_jet);
//		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
//			auto tauVisP4 = match_map[best_itau].first -> p4();
//			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
//			if (old_DR > best_DR){
//				match_map[best_itau] = std::pair<const std::unique_ptr<xAOD::IParticle>, const std::unique_ptr<xAOD::IParticle> >(best_tau, best_jet);
//			}
//		}//endl review
//
//	}//end of Jet Loop
//	return match_map;
//}

//std::unordered_map<int, std::pair< std::unique_ptr<const xAOD::IParticle> , std::unique_ptr<const xAOD::IParticle> > > Utils::findMatchDRI( 
//        std::vector< std::unique_ptr<const xAOD::IParticle> > jetCont , 
//		std::vector< std::unique_ptr<const xAOD::IParticle> > tauCont , float bestDR = 0.2 ){
//
//	std::unordered_map<int, std::pair< std::unique_ptr<const xAOD::IParticle> , std::unique_ptr<const xAOD::IParticle> > > match_map;
//	int ijet = -1;
//	int best_ijet = -1;
//    std::unique_ptr<const xAOD::IParticle> best_jet(nullptr);
//
//	for ( auto const& jet : jetCont) {
//		++ijet;
//		
//        std::unique_ptr<const xAOD::IParticle> best_tau(nullptr);
//		float currentDR = 0;
//		float best_DR = bestDR;
//		int itau = -1;
//		int best_itau = -1;
//
//		for ( auto const& tau : tauCont) {
//			++itau;
//			auto tauVisP4 = tau->p4();
//			//auto tauVisP4 = GetVisP4(tau);
//			currentDR = jet->p4().DeltaR(tauVisP4);
//
//			if (currentDR < best_DR){
//				best_DR = currentDR;
//				best_tau = std::move(tau);
//				best_jet = std::move(jet);
//				best_itau = itau;
//				best_ijet = ijet;
//			}
//		}//end of Tau Loop
//		
//		//if a new match is found for a previous 
//		//tau keep the new match if it is better 
//		std::unordered_map<int, std::pair< std::unique_ptr<const xAOD::IParticle> , std::unique_ptr<const xAOD::IParticle> > >::const_iterator got = match_map.find (best_itau);
//		if (got == match_map.end() and best_itau != -1 and best_ijet != -1) {
//			match_map[best_itau] = std::pair<const xAOD::IParticle*, const xAOD::IParticle*>(best_tau, best_jet);
//		}else if (got != match_map.end() and best_itau != -1 and best_ijet != -1){
//			auto tauVisP4 = match_map[best_itau].first -> p4();
//			//auto tauVisP4 = GetVisP4(match_map[best_itau].first);
//			float old_DR = match_map[best_itau].second->p4().DeltaR(tauVisP4);
//			if (old_DR > best_DR){
//				match_map[best_itau] = std::pair<const xAOD::IParticle*, const xAOD::IParticle* >(best_tau, best_jet);
//			}
//		}//endl review
//
//	}//end of Jet Loop
//	return match_map;
//}

