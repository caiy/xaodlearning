#include <MyAnalysis/TauEle.h>
#include <xAODEventInfo/EventInfo.h>
#include <AthLinks/ElementLink.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCore/AuxContainerBase.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

#include "MyAnalysis/Utils.h"

TauEle::TauEle(const std::string& name,
		ISvcLocator *pSvcLocator)
	: EL::AnaAlgorithm (name, pSvcLocator)
{
	//init variables
    //#include <AsgTools/MessageCheck.h>
    // declare the tool handle as a property on the algorithm
	//declareProperty ("grlTool", m_grl, "the GRL tool");
}

StatusCode TauEle::initialize()
{
	//Tool loaded?
	//ANA_CHECK (m_grl.retrieve());
	
	//histogram and ...
	//ANA_MSG_INFO ("in initialize");
	
	//ntuple
	ANA_CHECK (book (TTree ("analysis", "analysis ntuple")));
	TTree* mytree = tree ("analysis");
	ANA_CHECK (book (TH1F ("Matched_jetPt", "Matched_jetPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Truth_tauPt", "Truth_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Matched_tauPt1", "Matched_tauPt1", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Matched_tauPt2", "Matched_tauPt2", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("HadTau_DecayMode", "HadTau_DecayMode", 15, 0, 15))); // 5*(charged_pion-1)+neutral_pion max:5*(3-1)+4

    m_nMatched = 0;
	m_EMPFjetEta = new std::vector<double>();
	m_EMPFjetPhi = new std::vector<double>();
	m_EMPFjetPt  = new std::vector<double>();
	m_EMPFjetE   = new std::vector<double>();
	m_LCTopojetEta = new std::vector<double>();
	m_LCTopojetPhi = new std::vector<double>();
	m_LCTopojetPt  = new std::vector<double>();
	m_LCTopojetE   = new std::vector<double>();
	m_LCTopojetM   = new std::vector<double>();
	
    m_tauNum = new std::vector<int>();
	m_tauEta = new std::vector<double>();
	m_tauPhi = new std::vector<double>();
	m_tauPt  = new std::vector<double>();
	m_tauE   = new std::vector<double>();
	m_tauM   = new std::vector<double>();
	m_tauE_NPion = new std::vector<double>();
	
    m_EleEta = new std::vector<double>();
	m_ElePhi = new std::vector<double>();
	m_ElePt  = new std::vector<double>();
	m_EleE   = new std::vector<double>();
	m_EleM   = new std::vector<double>();
	
    m_MatchedEleEta = new std::vector<double>();
	m_MatchedElePhi = new std::vector<double>();
	m_MatchedElePt  = new std::vector<double>();
	m_MatchedEleE   = new std::vector<double>();
	m_MatchedEleM   = new std::vector<double>();
	m_MatchedtauEta = new std::vector<double>();
	m_MatchedtauPhi = new std::vector<double>();
	m_MatchedtauPt  = new std::vector<double>();
	m_MatchedtauE   = new std::vector<double>();
	m_MatchedtauM   = new std::vector<double>();
    
    m_MatchedjetEta = new std::vector<double>();
	m_MatchedjetPhi = new std::vector<double>();
	m_MatchedjetPt  = new std::vector<double>();
	m_MatchedjetE   = new std::vector<double>();
	m_MatchedjetM   = new std::vector<double>();
    m_MatchedjettauEta = new std::vector<double>();
	m_MatchedjettauPhi = new std::vector<double>();
	m_MatchedjettauPt  = new std::vector<double>();
	m_MatchedjettauE   = new std::vector<double>();
	m_MatchedjettauM   = new std::vector<double>();
	
	m_ChargedPion = new std::vector<size_t>();
	m_NeutralPion = new std::vector<size_t>();
	
	mytree->Branch ("RunNumber", &m_runNumber);
	mytree->Branch ("EventNumber", &m_eventNumber);
	
    mytree->Branch ("EMPFjets_eta", &m_EMPFjetEta);
	mytree->Branch ("EMPFjets_phi", &m_EMPFjetPhi);
	mytree->Branch ("EMPFjets_pt" , &m_EMPFjetPt );
	mytree->Branch ("EMPFjets_e"  , &m_EMPFjetE  );
	mytree->Branch ("EMPFjets_m"  , &m_EMPFjetM  );
	mytree->Branch ("LCTopojets_eta", &m_LCTopojetEta);
	mytree->Branch ("LCTopojets_phi", &m_LCTopojetPhi);
	mytree->Branch ("LCTopojets_pt" , &m_LCTopojetPt );
	mytree->Branch ("LCTopojets_e"  , &m_LCTopojetE  );
	mytree->Branch ("LCTopojets_m"  , &m_LCTopojetM  );
	
    mytree->Branch ("taus_number", &m_tauNum);
	mytree->Branch ("taus_eta", &m_tauEta);
	mytree->Branch ("taus_phi", &m_tauPhi);
	mytree->Branch ("taus_pt" , &m_tauPt );
	mytree->Branch ("taus_e"  , &m_tauE  );
	mytree->Branch ("taus_e_np"  , &m_tauE_NPion  );
	mytree->Branch ("taus_m"  , &m_tauM  );
	mytree->Branch ("taus_nChargedPion"  , &m_ChargedPion );//
	mytree->Branch ("taus_nNeutralPion"  , &m_NeutralPion );//
	//mytree->Branch ("taus_r"  , &m_tauR  );
	mytree->Branch ("ele_eta", &m_EleEta);
	mytree->Branch ("ele_phi", &m_ElePhi);
	mytree->Branch ("ele_pt" , &m_ElePt );
	mytree->Branch ("ele_e"  , &m_EleE  );
	mytree->Branch ("ele_m"  , &m_EleM  );
	//mytree->Branch ("ele_r"  , &m_EleR  );
	mytree->Branch ("MatchedEle_eta", &m_MatchedEleEta);
	mytree->Branch ("MatchedEle_phi", &m_MatchedElePhi);
	mytree->Branch ("MatchedEle_pt" , &m_MatchedElePt );
	mytree->Branch ("MatchedEle_e"  , &m_MatchedEleE  );
	mytree->Branch ("MatchedEle_m"  , &m_MatchedEleM  );
	
    mytree->Branch ("MatchedNum"  , &m_nMatched );
	//mytree->Branch ("MatchedEle_r"  , &m_MatchedEMPFjetR  );
	//mytree->Branch ("MatchedEle_nChargedTrack" , &m_EMPFChargedTrack);//
	//mytree->Branch ("MatchedEle_nChargedPFO" , &m_EMPFChargedPFO);//
	//mytree->Branch ("MatchedEle_nNeutralPFO" , &m_EMPFNeutralPFO);//
	mytree->Branch ("MatchedTaus_eta", &m_MatchedtauEta);
	mytree->Branch ("MatchedTaus_phi", &m_MatchedtauPhi);
	mytree->Branch ("MatchedTaus_pt" , &m_MatchedtauPt );
	mytree->Branch ("MatchedTaus_e"  , &m_MatchedtauE  );
	mytree->Branch ("MatchedTaus_m"  , &m_MatchedtauM  );
	//mytree->Branch ("MatchedEMPFtaus_r"  , &m_MatchedEMPFtauR  );
	mytree->Branch ("MatchedJets_eta", &m_MatchedjetEta);
	mytree->Branch ("MatchedJets_phi", &m_MatchedjetPhi);
	mytree->Branch ("MatchedJets_pt" , &m_MatchedjetPt );
	mytree->Branch ("MatchedJets_e"  , &m_MatchedjetE  );
	mytree->Branch ("MatchedJets_m"  , &m_MatchedjetM  );

	mytree->Branch ("MatchedJetsTaus_eta", &m_MatchedjettauEta);
	mytree->Branch ("MatchedJetsTaus_phi", &m_MatchedjettauPhi);
	mytree->Branch ("MatchedJetsTaus_pt" , &m_MatchedjettauPt );
	mytree->Branch ("MatchedJetsTaus_e"  , &m_MatchedjettauE  );
	mytree->Branch ("MatchedJetsTaus_m"  , &m_MatchedjettauM  );

	return StatusCode::SUCCESS;
}

StatusCode TauEle::execute()
{

	// retrieve the eventInfo object from the event store
	//const xAOD::EventInfo* eventInfo = 0;
	//ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
	//m_runNumber = eventInfo->runNumber();
	//m_eventNumber = eventInfo->eventNumber();
	m_runNumber = 0;
	m_eventNumber = 0;
	//for( const xAOD::EventInfo::SubEvent& subEv : eventInfo->subEvents() ){
	//}
	
	// check if the event is data or MC
	//bool isMC = false;
	//if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ){
	//	isMC = true;
	//}
	//if (!isMC){
	//	if (!m_grl->passRunLB(*eventInfo) ){
	//		ANA_MSG_INFO ("drop event: GRL");
	//		return StatusCode::SUCCESS; // go to next event
	//	}
	//}
	// write branch
	// print out run and event number from retrieved object
	//ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
	ANA_MSG_INFO ("retrive truth tau");
	//****************************************************************************
	//*** Attention! If you loop over the Truth particles / vertices directly, ***
	//*** you will get signal and pile-up together. So we use the Truth event. ***
	//****************************************************************************
	m_tauNum->clear();
	m_tauEta->clear();
	m_tauPhi->clear();
	m_tauPt->clear();
	m_tauE->clear();
	m_tauE_NPion->clear();
	m_tauM->clear();
	m_ChargedPion->clear();
	m_NeutralPion->clear();
	const xAOD::TruthEventContainer* xTruthEvtContainer = NULL;
	std::vector<const xAOD::TruthParticle*> xTruthTauContainer;
	//const xAOD::TruthParticleContainer* xTruthPContainer = NULL;
	ANA_CHECK (evtStore()->retrieve (xTruthEvtContainer, "TruthEvents"));
	for (auto truthEvt : *xTruthEvtContainer) {
		const std::vector<float> weights = truthEvt->weights();
		//float scale = truthEvt->eventScale();
		for (const auto& truthPLink : truthEvt->truthParticleLinks() ){
			if (!truthPLink.isValid()) continue;
			const xAOD::TruthParticle* truthP = *truthPLink;
			if (!truthP->isTau()) continue;
			Utils::RecoTauVisDecay( *truthP ); 
			if ( truthP->auxdata<int>("IsTauDecay") < 1 ) continue;
			if ( truthP->auxdata<int>("IsHadronicTau") < 1 ) continue;
			if ( Utils::PassCaloShape(truthP->auxdata<double>("eta_vis")) ) 
			{
				m_tauEta->push_back( truthP->auxdata<double>("eta_vis") );
				m_tauPhi->push_back( truthP->auxdata<double>("phi_vis") );
				m_tauPt ->push_back( truthP->auxdata<double>("pt_vis") );
				m_tauM  ->push_back( truthP->auxdata<double>("m_vis") );
				m_tauE  ->push_back( truthP->auxdata<double>("e_vis") );
				m_tauE_NPion  ->push_back( truthP->auxdata<double>("e_vis_neutral_pions") );
				m_ChargedPion->push_back(truthP->auxdata<size_t>("numChargedPion"));
				m_NeutralPion->push_back(truthP->auxdata<size_t>("numNeutralPion"));
				hist("HadTau_DecayMode")->Fill( (truthP->auxdata<size_t>("numChargedPion")-1)*5 + truthP->auxdata<size_t>("numNeutralPion") );
				hist("Truth_tauPt")->Fill( truthP->auxdata<double>("pt_vis") * 0.001 );
				xTruthTauContainer.push_back(truthP);
			}
		}
	}
	m_tauNum->push_back(m_tauEta->size());
	
    
	std::vector<const xAOD::IParticle*> vEMPF;
    const xAOD::JetContainer* EMPFjets = nullptr;
	ANA_CHECK (evtStore()->retrieve (EMPFjets, "AntiKt4EMPFlowJets"));
	m_EMPFjetEta->clear();
	m_EMPFjetPhi->clear();
	m_EMPFjetPt->clear();
	m_EMPFjetE->clear();
	m_EMPFjetM->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *EMPFjets) {
		m_EMPFjetEta->push_back( jet->eta() );
		m_EMPFjetPhi->push_back( jet->phi() );
		m_EMPFjetPt->push_back( jet->pt() );
		m_EMPFjetE->push_back( jet->e() );
		m_EMPFjetM->push_back( jet->m() );
	    vEMPF.push_back( jet );
    }
	// LCtopo jets cont
	std::vector<const xAOD::IParticle*> vLCTopo;
	const xAOD::JetContainer* LCTopojets = nullptr;
	ANA_CHECK (evtStore()->retrieve (LCTopojets, "AntiKt4LCTopoJets"));
	m_LCTopojetEta->clear();
	m_LCTopojetPhi->clear();
	m_LCTopojetPt->clear();
	m_LCTopojetE->clear();
	m_LCTopojetM->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *LCTopojets) {
		m_LCTopojetEta->push_back( jet->eta() );
		m_LCTopojetPhi->push_back( jet->phi() );
		m_LCTopojetPt->push_back( jet->pt() );
		m_LCTopojetE->push_back( jet->e() );
		m_LCTopojetM->push_back( jet->m() );
	    vLCTopo.push_back( jet );
    }


	//ANA_MSG_INFO ("retrive taujet");
	// Taujet cont
	const xAOD::ElectronContainer* eles = nullptr;
	std::vector<const xAOD::IParticle*> vEle;
	ANA_CHECK (evtStore()->retrieve (eles, "Electrons"));
	m_EleEta ->clear();
	m_ElePhi ->clear();
	m_ElePt  ->clear();
	m_EleE   ->clear();
	m_EleM   ->clear();
	for (auto ele : *eles) {
        if( ele->passSelection("LHMedium") ){
		    m_EleEta->push_back( ele->eta() );
		    m_ElePhi->push_back( ele->phi() );
		    m_ElePt->push_back(  ele->pt() );
		    m_EleE->push_back(   ele->e() );
		    m_EleM->push_back(   ele->m() );
		    vEle.push_back( ele );
        }
	}
	
	// loop over the jets in the container
	
	
	//tau jet match test
	auto TauEleMatch = Utils::findMatchDRI(vEle,xTruthTauContainer,0.2);
	m_MatchedEleEta->clear();
	m_MatchedElePhi->clear();
	m_MatchedElePt->clear();
	m_MatchedEleE->clear();
	m_MatchedEleM->clear();
	m_MatchedtauEta->clear();
	m_MatchedtauPhi->clear();
	m_MatchedtauPt->clear();
	m_MatchedtauE->clear();
	m_MatchedtauM->clear();
	std::vector<const xAOD::TruthParticle*> vTruthTau;
	for ( auto tauele : TauEleMatch ) {
		if ( Utils::PassCaloShape(tauele.second.second->eta()) && Utils::PassCaloShape(tauele.second.first->auxdata<double>("eta_vis")) ) 
		{
			hist("Matched_jetPt")->Fill(tauele.second.second->pt() * 0.001 );
			m_MatchedEleEta->push_back( tauele.second.second->eta() );
			m_MatchedElePhi->push_back( tauele.second.second->phi() );
			m_MatchedElePt ->push_back( tauele.second.second->pt() );
			m_MatchedEleE -> push_back( tauele.second.second->e() );
			m_MatchedEleM -> push_back( tauele.second.second->m() );
			hist("Matched_tauPt1")->Fill( tauele.second.first->auxdata<double>("pt_vis") * 0.001 );
			m_MatchedtauEta->push_back( tauele.second.first->auxdata<double>("eta_vis") );
			m_MatchedtauPhi->push_back( tauele.second.first->auxdata<double>("phi_vis") );
			m_MatchedtauPt ->push_back( tauele.second.first->auxdata<double>("pt_vis") );
			m_MatchedtauM  ->push_back( tauele.second.first->auxdata<double>("m_vis") );
			m_MatchedtauE  ->push_back( tauele.second.first->auxdata<double>("e_vis") );
		    vTruthTau.push_back(tauele.second.first);
        }
	}
    
	
    const xAOD::TauJetContainer* taus = nullptr;
	std::vector<const xAOD::TauJet*> vTauJet;
	ANA_CHECK (evtStore()->retrieve (taus, "TauJets"));
	for (auto taujet : *taus) {
		vTauJet.push_back( taujet );
	}
	
	
    //auto match = Utils::findMatchDRI(vEMPF,vTruthTau,0.2);
    auto match = Utils::findMatchDRI(vLCTopo,vTruthTau,0.2);
	m_MatchedjetEta->clear();
	m_MatchedjetPhi->clear();
	m_MatchedjetPt->clear();
	m_MatchedjetE->clear();
	m_MatchedjetM->clear();
	m_MatchedjettauEta->clear();
	m_MatchedjettauPhi->clear();
	m_MatchedjettauPt->clear();
	m_MatchedjettauE->clear();
	m_MatchedjettauM->clear();
	m_nMatched = match.size();
	for ( auto taujetsjet : match ) {
		if ( Utils::PassCaloShape(taujetsjet.second.second->eta()) && Utils::PassCaloShape(taujetsjet.second.first->auxdata<double>("eta_vis")) ) 
		{
			hist("Matched_jetPt")->Fill( taujetsjet.second.second->pt() * 0.001 );
			m_MatchedjetEta->push_back( taujetsjet.second.second->eta() );
			m_MatchedjetPhi->push_back( taujetsjet.second.second->phi() );
			m_MatchedjetPt ->push_back( taujetsjet.second.second->pt() );
			m_MatchedjetE -> push_back( taujetsjet.second.second->e() );
			m_MatchedjetM -> push_back( taujetsjet.second.second->m() );
			hist("Matched_tauPt2")->Fill( taujetsjet.second.first->auxdata<double>("pt_vis") * 0.001 );
			m_MatchedjettauEta->push_back( taujetsjet.second.first->auxdata<double>("eta_vis") );
			m_MatchedjettauPhi->push_back( taujetsjet.second.first->auxdata<double>("phi_vis") );
			m_MatchedjettauPt ->push_back( taujetsjet.second.first->auxdata<double>("pt_vis") );
			m_MatchedjettauM  ->push_back( taujetsjet.second.first->auxdata<double>("m_vis") );
			m_MatchedjettauE  ->push_back( taujetsjet.second.first->auxdata<double>("e_vis") );
		}
	}

	// last!
	tree ("analysis")->Fill ();
	
	return StatusCode::SUCCESS;
}

StatusCode TauEle::finalize()
{
	//write
	return StatusCode::SUCCESS;
}


TauEle::~TauEle()
{
	delete m_EMPFjetEta;
	delete m_EMPFjetPhi;
	delete m_EMPFjetPt ;
	delete m_EMPFjetE  ;
	delete m_EMPFjetM  ;
	delete m_LCTopojetEta;
	delete m_LCTopojetPhi;
	delete m_LCTopojetPt ;
	delete m_LCTopojetE  ;
	delete m_LCTopojetM  ;
	
    delete m_tauNum ;
	delete m_tauEta ;
	delete m_tauPhi ;
	delete m_tauPt  ;
	delete m_tauE   ;
	delete m_tauE_NPion ;
	delete m_tauM   ;
	delete m_ChargedPion ;
	delete m_NeutralPion ;
	 
	delete m_EleEta ;
	delete m_ElePhi ;
	delete m_ElePt  ;
	delete m_EleE   ;
	delete m_EleM   ;
	
    delete m_MatchedjetEta;
	delete m_MatchedjetPhi;
	delete m_MatchedjetPt ;
	delete m_MatchedjetE  ;
	delete m_MatchedjetM  ;
	
    delete m_MatchedjettauEta;
	delete m_MatchedjettauPhi;
	delete m_MatchedjettauPt ;
	delete m_MatchedjettauE  ;
	delete m_MatchedjettauM  ;

    delete m_MatchedEleEta ;
	delete m_MatchedElePhi ;
	delete m_MatchedElePt  ;
	delete m_MatchedEleE   ;
	delete m_MatchedEleM   ;
	 
	delete m_MatchedtauEta ;
	delete m_MatchedtauPhi ;
	delete m_MatchedtauPt  ;
	delete m_MatchedtauE   ;
	delete m_MatchedtauM   ;
	
}
