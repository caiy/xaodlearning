#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>

#include <MyAnalysis/Utils.h>

MyxAODAnalysis::MyxAODAnalysis(const std::string& name,
		ISvcLocator *pSvcLocator)
	: EL::AnaAlgorithm (name, pSvcLocator)
{
	//init variables

	// declare the tool handle as a property on the algorithm
	//declareProperty ("grlTool", m_grl, "the GRL tool");
//#include <AsgTools/MessageCheck.h>
}

StatusCode MyxAODAnalysis::initialize()
{
	//Tool loaded?
	//ANA_CHECK (m_grl.retrieve());
	
	//histogram and ...
	ANA_MSG_INFO ("in initialize");
	ANA_CHECK (book (TH1F ("AntiKt4EMPF_jetPt", "AntiKt4_EMPFlow_jetPt", 20, 0, 100))); // jet pt [GeV]
	
	//ntuple
	ANA_CHECK (book (TTree ("analysis", "analysis ntuple")));
	TTree* mytree = tree ("analysis");

	m_jetEta = new std::vector<float>();
	m_jetPhi = new std::vector<float>();
	m_jetPt  = new std::vector<float>();
	m_jetE   = new std::vector<float>();
	
	mytree->Branch ("RunNumber", &m_runNumber);
	mytree->Branch ("EventNumber", &m_eventNumber);
	mytree->Branch ("JetEta", &m_jetEta);
	mytree->Branch ("JetPhi", &m_jetPhi);
	mytree->Branch ("JetPt", &m_jetPt);
	mytree->Branch ("JetE", &m_jetE);
		
	return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis::execute()
{
	//every events
	//ANA_MSG_INFO ("in execute");

	// retrieve the eventInfo object from the event store
	//const xAOD::EventInfo *eventInfo = nullptr;
	//ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
	
	// check if the event is data or MC
	//bool isMC = false;
	//if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ){
	//	isMC = true;
	//}
	//if (!isMC){
	//	if (!m_grl->passRunLB(*eventInfo) ){
	//		ANA_MSG_INFO ("drop event: GRL");
	//		return StatusCode::SUCCESS; // go to next event
	//	}
	//}
	// write branch
	//m_runNumber = eventInfo->runNumber();
	//m_eventNumber = eventInfo->eventNumber();
	// print out run and event number from retrieved object
	//ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

	// loop over the jets in the container
	const xAOD::JetContainer* jets = nullptr;
	ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMPFlowJets"));
	m_jetEta->clear();
	m_jetPhi->clear();
	m_jetPt->clear();
	m_jetE->clear();
	//for (const xAOD::Jet* jet : *jets) {}
	for (auto jet : *jets) {
		hist ("AntiKt4EMPF_jetPt")->Fill( jet->pt() * 0.001 ); // GeV
		m_jetEta->push_back( jet->eta() );
		m_jetPhi->push_back( jet->phi() );
		m_jetPt->push_back( jet->pt() );
		m_jetE->push_back( jet->e() );
	}
	// Fill the event into the tree:
	tree ("analysis")->Fill ();

	return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis::finalize()
{
	//write
	return StatusCode::SUCCESS;
}

MyxAODAnalysis::~MyxAODAnalysis()
{
	delete m_jetEta;
	delete m_jetPhi;
	delete m_jetPt;
	delete m_jetE;
}
