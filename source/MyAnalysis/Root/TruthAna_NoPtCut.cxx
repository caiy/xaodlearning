#include <MyAnalysis/TruthAna_NoPtCut.h>
#include <xAODEventInfo/EventInfo.h>
#include <AthLinks/ElementLink.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCore/AuxContainerBase.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

#include "MyAnalysis/Utils.h"

TruthAna_NoPtCut::TruthAna_NoPtCut(const std::string& name,
		ISvcLocator *pSvcLocator)
	: EL::AnaAlgorithm (name, pSvcLocator)
{
	//init variables
    //#include <AsgTools/MessageCheck.h>
	// declare the tool handle as a property on the algorithm
	//declareProperty ("grlTool", m_grl, "the GRL tool");
}

StatusCode TruthAna_NoPtCut::initialize()
{
	//Tool loaded?
	//ANA_CHECK (m_grl.retrieve());
	
	//histogram and ...
	//ANA_MSG_INFO ("in initialize");
	
	//ntuple
	ANA_CHECK (book (TTree ("analysis", "analysis ntuple")));
	TTree* mytree = tree ("analysis");
	ANA_CHECK (book (TH1F ("jetSeedPt", "jetSeedPt", 20, 0, 20))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Matched_jetPt", "Matched_jetPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Truth_tauPt", "Truth_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("Matched_tauPt", "Matched_tauPt", 50, 0, 50))); // jet pt [GeV]
	ANA_CHECK (book (TH1F ("HadTau_DecayMode", "HadTau_DecayMode", 15, 0, 15))); // 5*(charged_pion-1)+neutral_pion max:5*(3-1)+4
    
    m_nMatched = 0;
	m_tauNum = new std::vector<int>();
	m_tauEta = new std::vector<double>();
	m_tauPhi = new std::vector<double>();
	m_tauPt  = new std::vector<double>();
	m_tauE   = new std::vector<double>();
	m_tauM   = new std::vector<double>();
	m_tauR   = new std::vector<double>();
	m_tauE_NPion = new std::vector<double>();
	m_tauJetEta = new std::vector<double>();
	m_tauJetPhi = new std::vector<double>();
	m_tauJetPt  = new std::vector<double>();
	m_tauJet_seedPt  = new std::vector<double>();
	m_tauJetE   = new std::vector<double>();
	m_tauJetM   = new std::vector<double>();
	m_tauJetR   = new std::vector<double>();
	
    m_MatchedjetEta = new std::vector<double>();
	m_MatchedjetPhi = new std::vector<double>();
	m_MatchedjetPt  = new std::vector<double>();
	m_MatchedjetE   = new std::vector<double>();
	m_MatchedjetM   = new std::vector<double>();
	m_MatchedjetR   = new std::vector<double>();
    
    m_MatchedjetChargedTrack = new std::vector<size_t>();
	m_MatchedjetChargedPFO = new std::vector<size_t>();
	m_MatchedjetNeutralPFO = new std::vector<size_t>();
	
    m_MatchedtauEta = new std::vector<double>();
	m_MatchedtauPhi = new std::vector<double>();
	m_MatchedtauPt  = new std::vector<double>();
	m_MatchedtauE   = new std::vector<double>();
	m_MatchedtauE_NPion = new std::vector<double>();
	m_MatchedtauM   = new std::vector<double>();
	m_MatchedtauR   = new std::vector<double>();
	
	m_ChargedPion = new std::vector<size_t>();
	m_NeutralPion = new std::vector<size_t>();
	m_MatchedtauChargedPion = new std::vector<size_t>();
	m_MatchedtauNeutralPion = new std::vector<size_t>();
	
	mytree->Branch ("RunNumber", &m_runNumber);
	mytree->Branch ("EventNumber", &m_eventNumber);
	mytree->Branch ("taus_number", &m_tauNum);
	mytree->Branch ("taus_eta", &m_tauEta);
	mytree->Branch ("taus_phi", &m_tauPhi);
	mytree->Branch ("taus_pt" , &m_tauPt );
	mytree->Branch ("taus_e"  , &m_tauE  );
	mytree->Branch ("taus_e_np"  , &m_tauE_NPion  );
	mytree->Branch ("taus_m"  , &m_tauM  );
	mytree->Branch ("taus_nChargedPion"  , &m_ChargedPion );//
	mytree->Branch ("taus_nNeutralPion"  , &m_NeutralPion );//
	//mytree->Branch ("taus_r"  , &m_tauR  );
	mytree->Branch ("taujets_eta", &m_tauJetEta);
	mytree->Branch ("taujets_phi", &m_tauJetPhi);
	mytree->Branch ("taujets_pt" , &m_tauJetPt );
	mytree->Branch ("taujets_seed_pt" , &m_tauJet_seedPt );
	mytree->Branch ("taujets_e"  , &m_tauJetE  );
	mytree->Branch ("taujets_m"  , &m_tauJetM  );
	mytree->Branch ("taujets_r"  , &m_tauJetR  );
	
    mytree->Branch ("MatchedNum" , &m_nMatched);//
	
    mytree->Branch ("Matchedjets_eta", &m_MatchedjetEta);
	mytree->Branch ("Matchedjets_phi", &m_MatchedjetPhi);
	mytree->Branch ("Matchedjets_pt" , &m_MatchedjetPt );
	mytree->Branch ("Matchedjets_e"  , &m_MatchedjetE  );
	mytree->Branch ("Matchedjets_m"  , &m_MatchedjetM  );
	mytree->Branch ("Matchedjets_r"  , &m_MatchedjetR  );
	mytree->Branch ("Matchedjets_nChargedTrack",&m_MatchedjetChargedTrack);//
	mytree->Branch ("Matchedjets_nChargedPFO" , &m_MatchedjetChargedPFO);//
	mytree->Branch ("Matchedjets_nNeutralPFO" , &m_MatchedjetNeutralPFO);//
	
	mytree->Branch ("Matchedtaus_eta", &m_MatchedtauEta);
	mytree->Branch ("Matchedtaus_phi", &m_MatchedtauPhi);
	mytree->Branch ("Matchedtaus_pt" , &m_MatchedtauPt );
	mytree->Branch ("Matchedtaus_e"  , &m_MatchedtauE  );
	mytree->Branch ("Matchedtaus_m"  , &m_MatchedtauM  );
	mytree->Branch ("Matchedtaus_nChargedPion"  , &m_MatchedtauChargedPion );
	mytree->Branch ("Matchedtaus_nNeutralPion"  , &m_MatchedtauNeutralPion );
	//mytree->Branch ("MatchedEMPFtaus_r"  , &m_MatchedEMPFtauR  );
	
	return StatusCode::SUCCESS;
}

StatusCode TruthAna_NoPtCut::execute()
{

	// retrieve the eventInfo object from the event store
	//const xAOD::EventInfo* eventInfo = 0;
	//ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
	//m_runNumber = eventInfo->runNumber();
	//m_eventNumber = eventInfo->eventNumber();
	m_runNumber = 0;
	m_eventNumber = 0;
	//for( const xAOD::EventInfo::SubEvent& subEv : eventInfo->subEvents() ){
	//}
	
	// check if the event is data or MC
	//bool isMC = false;
	//if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ){
	//	isMC = true;
	//}
	//if (!isMC){
	//	if (!m_grl->passRunLB(*eventInfo) ){
	//		ANA_MSG_INFO ("drop event: GRL");
	//		return StatusCode::SUCCESS; // go to next event
	//	}
	//}
	// write branch
	// print out run and event number from retrieved object
	//ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
	//ANA_MSG_INFO ("retrive truth tau");
	//****************************************************************************
	//*** Attention! If you loop over the Truth particles / vertices directly, ***
	//*** you will get signal and pile-up together. So we use the Truth event. ***
	//****************************************************************************
	m_tauNum->clear();
	m_tauEta->clear();
	m_tauPhi->clear();
	m_tauPt->clear();
	m_tauE->clear();
	m_tauE_NPion->clear();
	m_tauM->clear();
	m_tauR->clear();
	m_ChargedPion->clear();
	m_NeutralPion->clear();
	const xAOD::TruthEventContainer* xTruthEvtContainer = NULL;
	std::vector<const xAOD::TruthParticle*> xTruthTauContainer;
	//const xAOD::TruthParticleContainer* xTruthPContainer = NULL;
	ANA_CHECK (evtStore()->retrieve (xTruthEvtContainer, "TruthEvents"));
	for (auto truthEvt : *xTruthEvtContainer) {
		const std::vector<float> weights = truthEvt->weights();
		//float scale = truthEvt->eventScale();
		for (const auto& truthPLink : truthEvt->truthParticleLinks() ){
			if (!truthPLink.isValid()) continue;
			const xAOD::TruthParticle* truthP = *truthPLink;
			if (!truthP->isTau()) continue;
			Utils::RecoTauVisDecay( *truthP ); 
			if ( truthP->auxdata<int>("IsTauDecay") < 1 ) continue;
			if ( truthP->auxdata<int>("IsHadronicTau") < 1 ) continue;
			//if ( Utils::PassCaloShape(truthP->auxdata<double>("eta_vis")) && truthP->auxdata<double>("pt_vis") * 0.001 >=10 ) 
			if ( truthP->auxdata<double>("pt_vis") * 0.001 >=10 && 
                    fabs(truthP->auxdata<double>("eta_vis"))<2.5 )  
			{
				m_tauEta->push_back( truthP->auxdata<double>("eta_vis") );
				m_tauPhi->push_back( truthP->auxdata<double>("phi_vis") );
				m_tauPt ->push_back( truthP->auxdata<double>("pt_vis") );
				m_tauM  ->push_back( truthP->auxdata<double>("m_vis") );
				m_tauE  ->push_back( truthP->auxdata<double>("e_vis") );
				m_tauE_NPion  ->push_back( truthP->auxdata<double>("e_vis_neutral_pions") );
				m_tauR  ->push_back( truthP->rapidity() );
				m_ChargedPion->push_back(truthP->auxdata<size_t>("numChargedPion"));
				m_NeutralPion->push_back(truthP->auxdata<size_t>("numNeutralPion"));
				hist("Truth_tauPt")->Fill( truthP->auxdata<double>("pt_vis") * 0.001 );
				xTruthTauContainer.push_back(truthP);
			}
		}
	}
	m_tauNum->push_back(m_tauEta->size());

	//ANA_MSG_INFO ("retrive taujet");
	// Taujet cont
	const xAOD::TauJetContainer* taus = nullptr;
	std::vector<const xAOD::TauJet*> vTauJet;
	ANA_CHECK (evtStore()->retrieve (taus, "TauJets"));
	m_tauJetEta ->clear();
	m_tauJetPhi ->clear();
	m_tauJetPt  ->clear();
	m_tauJet_seedPt ->clear();
	m_tauJetE   ->clear();
	m_tauJetM   ->clear();
	m_tauJetR   ->clear();
	for (auto taujet : *taus) {
		if ( taujet->ptJetSeed()*0.001 >= 1 && taujet->nTracksCharged()>=1 ) {
                //&& taujet->isTau(xAOD::TauJetParameters::IsTauFlag) 
			m_tauJetEta->push_back( taujet->eta() );
			m_tauJetPhi->push_back( taujet->phi() );
			m_tauJetPt->push_back(  taujet->pt() );
			m_tauJet_seedPt->push_back(  taujet->ptJetSeed() );
			m_tauJetE->push_back(   taujet->e() );
			m_tauJetM->push_back(   taujet->m() );
			m_tauJetR->push_back(   taujet->rapidity() );
			vTauJet.push_back( taujet );
			hist("jetSeedPt")->Fill( taujet->ptJetSeed() * 0.001 );
		}
	}
	
	
	//tau jet match test
	auto match = Utils::findMatchDR(vTauJet,xTruthTauContainer,0.2);
	m_MatchedjetEta->clear();
	m_MatchedjetPhi->clear();
	m_MatchedjetPt->clear();
	m_MatchedjetE->clear();
	m_MatchedjetM->clear();
	m_MatchedjetR->clear();
	m_MatchedjetChargedTrack->clear();
	m_MatchedjetChargedPFO->clear();
	m_MatchedjetNeutralPFO->clear();
	m_MatchedtauEta->clear();
	m_MatchedtauPhi->clear();
	m_MatchedtauPt->clear();
	m_MatchedtauE->clear();
	m_MatchedtauE_NPion->clear();
	m_MatchedtauM->clear();
	m_MatchedtauR->clear();
	m_MatchedtauChargedPion->clear();
	m_MatchedtauNeutralPion->clear();
	m_nMatched = match.size();
	for ( auto taujetsjet : match ) {
		//if ( Utils::PassCaloShape(taujetsjet.second.second->eta()) && Utils::PassCaloShape(taujetsjet.second.first->auxdata<double>("eta_vis")) ) 
		if ( true ) 
		{
			hist("Matched_jetPt")->Fill( taujetsjet.second.second->pt() * 0.001 );
			m_MatchedjetEta->push_back( taujetsjet.second.second->eta() );
			m_MatchedjetPhi->push_back( taujetsjet.second.second->phi() );
			m_MatchedjetPt ->push_back( taujetsjet.second.second->pt() );
			m_MatchedjetE -> push_back( taujetsjet.second.second->e() );
			m_MatchedjetM -> push_back( taujetsjet.second.second->m() );
			m_MatchedjetR -> push_back( taujetsjet.second.second->rapidity() );
			m_MatchedjetChargedTrack-> push_back( taujetsjet.second.second->nTracksCharged() );
			m_MatchedjetChargedPFO-> push_back( taujetsjet.second.second->nChargedPFOs() );
			m_MatchedjetNeutralPFO-> push_back( taujetsjet.second.second->nPi0PFOs() );
			hist("Matched_tauPt")->Fill( taujetsjet.second.first->auxdata<double>("pt_vis") * 0.001 );
			m_MatchedtauEta->push_back( taujetsjet.second.first->auxdata<double>("eta_vis") );
			m_MatchedtauPhi->push_back( taujetsjet.second.first->auxdata<double>("phi_vis") );
			m_MatchedtauPt ->push_back( taujetsjet.second.first->auxdata<double>("pt_vis") );
			m_MatchedtauM  ->push_back( taujetsjet.second.first->auxdata<double>("m_vis") );
			m_MatchedtauE  ->push_back( taujetsjet.second.first->auxdata<double>("e_vis") );
			m_MatchedtauR  ->push_back( taujetsjet.second.first->rapidity() );
			m_MatchedtauChargedPion->push_back( taujetsjet.second.first->auxdata<size_t>("numChargedPion"));
			m_MatchedtauNeutralPion->push_back( taujetsjet.second.first->auxdata<size_t>("numNeutralPion"));
		}
	}
	

	// last!
	tree ("analysis")->Fill ();
	
	return StatusCode::SUCCESS;
}

StatusCode TruthAna_NoPtCut::finalize()
{
	//write
	return StatusCode::SUCCESS;
}


TruthAna_NoPtCut::~TruthAna_NoPtCut()
{
	delete m_tauNum;
	delete m_tauEta;
	delete m_tauPhi;
	delete m_tauPt ;
	delete m_tauE  ;
	delete m_tauE_NPion  ;
	delete m_tauM  ;
	delete m_tauR  ;
	delete m_tauJetEta;
	delete m_tauJetPhi;
	delete m_tauJetPt ;
	delete m_tauJet_seedPt ;
	delete m_tauJetE  ;
	delete m_tauJetM  ;
	delete m_tauJetR  ;
	delete m_MatchedjetEta;
	delete m_MatchedjetPhi;
	delete m_MatchedjetPt ;
	delete m_MatchedjetE  ;
	delete m_MatchedjetM  ;
	delete m_MatchedjetR  ;
	delete m_MatchedjetChargedTrack ;
	delete m_MatchedjetChargedPFO ;
	delete m_MatchedjetNeutralPFO ;
	delete m_MatchedtauEta;
	delete m_MatchedtauPhi;
	delete m_MatchedtauPt ;
	delete m_MatchedtauE  ;
	delete m_MatchedtauE_NPion ;
	delete m_MatchedtauM  ;
	delete m_MatchedtauR  ;
	delete m_MatchedtauChargedPion;
	delete m_MatchedtauNeutralPion;
}
