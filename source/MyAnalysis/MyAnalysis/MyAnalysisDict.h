#ifndef MYANALYSIS_MYANALYSIS_DICT_H
#define MYANALYSIS_MYANALYSIS_DICT_H

// This file includes all the header files that you need to create dictionaries for.

#include <MyAnalysis/MyxAODAnalysis.h>
#include <MyAnalysis/TruthAna.h>
#include <MyAnalysis/TruthAna_SeedPtCut.h>
#include <MyAnalysis/TruthAna_NoPtCut.h>
#include <MyAnalysis/TauEle.h>
#include <MyAnalysis/LinkCheck.h>

#endif
