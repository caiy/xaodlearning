#ifndef MyAnalysis_LinkCheck_H
#define MyAnalysis_LinkCheck_H

#include <xAODEgamma/ElectronContainer.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>

#include <xAODTau/TauJet.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODTau/TauDefs.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>

#include <xAODTruth/TruthEvent.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEventAuxContainer.h>

#include <xAODTruth/TruthPileupEvent.h>
#include <xAODTruth/TruthPileupEventContainer.h>
#include <xAODTruth/TruthPileupEventAuxContainer.h>

#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticleAuxContainer.h>

#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthVertexAuxContainer.h>

#include <xAODBase/IParticle.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

class LinkCheck : public EL::AnaAlgorithm
{
public:
	
	LinkCheck (const std::string& name, ISvcLocator* pSvcLocator);

	// these are the functions inherited from Algorithm
	virtual StatusCode initialize () override;
	virtual StatusCode execute () override;
	virtual StatusCode finalize () override;
	

	~LinkCheck () override;

	//GRL
    //#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
    //#include <AsgTools/ToolHandle.h>
	//ToolHandle<IGoodRunsListSelectionTool> m_grl;
	// Global info
	unsigned int m_runNumber = 0; ///< Run number
	unsigned long long m_eventNumber = 0; ///< Event number
	
	int m_nMatched = 0;
	// Jet info
	std::vector<double> *m_EMPFjetEta = nullptr;
	std::vector<double> *m_EMPFjetPhi = nullptr;
	std::vector<double> *m_EMPFjetPt  = nullptr;
	std::vector<double> *m_EMPFjetE   = nullptr;
	std::vector<double> *m_EMPFjetM   = nullptr;
	std::vector<double> *m_LCTopojetEta = nullptr;
	std::vector<double> *m_LCTopojetPhi = nullptr;
	std::vector<double> *m_LCTopojetPt  = nullptr;
	std::vector<double> *m_LCTopojetE   = nullptr;
	std::vector<double> *m_LCTopojetM   = nullptr;
	//Truth tau
	std::vector<int> *m_tauNum = nullptr;
	std::vector<double> *m_tauEta = nullptr;
	std::vector<double> *m_tauPhi = nullptr;
	std::vector<double> *m_tauPt  = nullptr;
	std::vector<double> *m_tauE   = nullptr;
	std::vector<double> *m_tauE_NPion = nullptr;
	std::vector<double> *m_tauM   = nullptr;
	std::vector<size_t> *m_ChargedPion = nullptr;//
	std::vector<size_t> *m_NeutralPion = nullptr;//
	//taujet
	std::vector<double> *m_tauJetEta = nullptr;
	std::vector<double> *m_tauJetPhi = nullptr;
	std::vector<double> *m_tauJetPt  = nullptr;
	std::vector<double> *m_tauJetE   = nullptr;
	std::vector<double> *m_tauJetM   = nullptr;
	//matched taujet
	std::vector<double> *m_MatchedJetTauEta = nullptr;
	std::vector<double> *m_MatchedJetTauPhi = nullptr;
	std::vector<double> *m_MatchedJetTauPt  = nullptr;
	std::vector<double> *m_MatchedJetTauE   = nullptr;
	std::vector<double> *m_MatchedJetTauM   = nullptr;
	//matched jet
    std::vector<double> *m_MatchedjetEta = nullptr;
	std::vector<double> *m_MatchedjetPhi = nullptr;
	std::vector<double> *m_MatchedjetPt  = nullptr;
	std::vector<double> *m_MatchedjetE   = nullptr;
	std::vector<double> *m_MatchedjetM   = nullptr;
    std::vector<double> *m_MatchedtauEta = nullptr;
	std::vector<double> *m_MatchedtauPhi = nullptr;
	std::vector<double> *m_MatchedtauPt  = nullptr;
	std::vector<double> *m_MatchedtauE   = nullptr;
	std::vector<double> *m_MatchedtauM   = nullptr;

private:

};

#endif
