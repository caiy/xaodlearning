#ifndef MYANALYSIS_UTILS_H
#define MYANALYSIS_UTILS_H

#include <xAODEgamma/ElectronContainer.h>
#include <xAODTau/TauJet.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>

#include <AthLinks/ElementLink.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODCore/AuxContainerBase.h>

#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthVertexAuxContainer.h>

#include <xAODBase/IParticle.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>
#include <TLorentzVector.h>

//********************************************************************************
//https://github.com/UCATLAS/xAODAnaHelpers/blob/master/Root/TauJetMatching.cxx **
//Thanks to UCATLAS/xAODAnaHelpers                                              **
//********************************************************************************
//#include <AsgTools/StatusCode.h>

class Utils 
{
public:
	Utils();

	// Reconstuction Tau Decay
	static bool RecoTauVisDecay(const xAOD::TruthParticle& xTruthParticle); 

	// Get Tau Decay Visible pT
	static TLorentzVector GetVisP4(const xAOD::TruthParticle* tauCont);

	static TLorentzVector GetInvisP4(const xAOD::TruthParticle* tauCont);
	
	static bool PassCaloShape(double eta);
	
//    static std::unordered_map<int, std::pair<const std::unique_ptr<xAOD::IParticle>, const std::unique_ptr<xAOD::IParticle> > > findMatchDRI( std::vector<const xAOD::IParticle* > jetCont , 
//			std::vector<const xAOD::IParticle* > tauCont , 
//			float bestDR );
    static std::unordered_map<int, std::pair<const xAOD::IParticle* , const xAOD::IParticle* > > findMatchDRI( std::vector<const xAOD::IParticle* > jetCont , 
			std::vector<const xAOD::IParticle* > tauCont , 
			float bestDR );
    
    static std::unordered_map<int, std::pair<const xAOD::TruthParticle* , const xAOD::IParticle* > > findMatchDRI( std::vector<const xAOD::IParticle* > jetCont , 
			std::vector<const xAOD::TruthParticle* > tauCont , 
			float bestDR );
    
//    static std::unordered_map<int, std::pair< std::unique_ptr<const xAOD::IParticle> , std::unique_ptr<const xAOD::IParticle> > > findMatchDRI( std::vector< std::unique_ptr<const xAOD::IParticle> > jetCont , 
//			std::vector< std::unique_ptr<const xAOD::IParticle> > tauCont , 
//			float bestDR );
    
    //**************************//
	static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Electron* > > findMatchDR( std::vector<const xAOD::Electron*> eleCont , 
			std::vector<const xAOD::TruthParticle*> tauCont , 
			float bestDR );

    static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet*    > > findMatchDR( std::vector<const xAOD::Jet*   > jetCont , 
			std::vector<const xAOD::TruthParticle*> tauCont , 
			float bestDR );

	static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > findMatchDR( std::vector<const xAOD::TauJet*> jetCont , 
			std::vector<const xAOD::TruthParticle*> tauCont , 
			float bestDR );
	
	static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > findMatchDR(const xAOD::TauJetContainer* jetCont , 
			std::vector<const xAOD::TruthParticle*> tauCont , 
			float bestDR );

	static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::TauJet* > > findMatchDR(const xAOD::TauJetContainer* jetCont , 
			const xAOD::TruthParticleContainer* tauCont , 
			float bestDR );

	static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > findMatchDR(const xAOD::JetContainer* jetCont , 
			const xAOD::TruthParticleContainer* tauCont , 
			float bestDR );
	
	static std::unordered_map<int, std::pair<const xAOD::TruthParticle*, const xAOD::Jet* > > findMatchDR(const xAOD::JetContainer* jetCont , 
			std::vector<const xAOD::TruthParticle*> tauCont , 
			float bestDR );


	~Utils();

private:

};

#endif
