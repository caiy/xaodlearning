#ifndef MyAnalysis_TruthAna_H
#define MyAnalysis_TruthAna_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>

#include <xAODTau/TauJet.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODTau/TauDefs.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>

#include <xAODTruth/TruthEvent.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEventAuxContainer.h>

#include <xAODTruth/TruthPileupEvent.h>
#include <xAODTruth/TruthPileupEventContainer.h>
#include <xAODTruth/TruthPileupEventAuxContainer.h>

#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticleAuxContainer.h>

#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthVertexAuxContainer.h>

#include <xAODBase/IParticle.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

class TruthAna : public EL::AnaAlgorithm
{
public:
	
	TruthAna (const std::string& name, ISvcLocator* pSvcLocator);

	// these are the functions inherited from Algorithm
	virtual StatusCode initialize () override;
	virtual StatusCode execute () override;
	virtual StatusCode finalize () override;
	

	~TruthAna () override;

	//GRL
    //#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
    //#include <AsgTools/ToolHandle.h>
	//ToolHandle<IGoodRunsListSelectionTool> m_grl;
	// Global info
	unsigned int m_runNumber = 0; ///< Run number
	unsigned long long m_eventNumber = 0; ///< Event number
	
	int m_nLCTopo = 0;
	int m_nEMPF = 0;
	// Jet info
	std::vector<double> *m_EMPFjetEta = nullptr;
	std::vector<double> *m_EMPFjetPhi = nullptr;
	std::vector<double> *m_EMPFjetPt  = nullptr;
	std::vector<double> *m_EMPFjetE   = nullptr;
	std::vector<double> *m_EMPFjetM   = nullptr;
	std::vector<double> *m_EMPFjetR   = nullptr;
	std::vector<double> *m_LCTopojetEta = nullptr;
	std::vector<double> *m_LCTopojetPhi = nullptr;
	std::vector<double> *m_LCTopojetPt  = nullptr;
	std::vector<double> *m_LCTopojetE   = nullptr;
	std::vector<double> *m_LCTopojetM   = nullptr;
	std::vector<double> *m_LCTopojetR   = nullptr;
	//Truth tau
	std::vector<int> *m_tauNum = nullptr;
	std::vector<double> *m_tauEta = nullptr;
	std::vector<double> *m_tauPhi = nullptr;
	std::vector<double> *m_tauPt  = nullptr;
	std::vector<double> *m_tauE   = nullptr;
	std::vector<double> *m_tauE_NPion = nullptr;
	std::vector<double> *m_tauM   = nullptr;
	std::vector<double> *m_tauR   = nullptr;
	std::vector<size_t> *m_ChargedPion = nullptr;//
	std::vector<size_t> *m_NeutralPion = nullptr;//
	//taujet
	std::vector<double> *m_tauJetEta = nullptr;
	std::vector<double> *m_tauJetPhi = nullptr;
	std::vector<double> *m_tauJetPt  = nullptr;
	std::vector<double> *m_tauJet_seedPt  = nullptr;
	std::vector<double> *m_tauJetE   = nullptr;
	std::vector<double> *m_tauJetM   = nullptr;
	std::vector<double> *m_tauJetR   = nullptr;
	//matched taujet
	std::vector<double> *m_MatchedEMPFjetEta = nullptr;
	std::vector<double> *m_MatchedEMPFjetPhi = nullptr;
	std::vector<double> *m_MatchedEMPFjetPt  = nullptr;
	std::vector<double> *m_MatchedEMPFjetE   = nullptr;
	std::vector<double> *m_MatchedEMPFjetM   = nullptr;
	std::vector<double> *m_MatchedEMPFjetR   = nullptr;
	std::vector<size_t> *m_EMPFChargedTrack  = nullptr;//
	std::vector<size_t> *m_EMPFChargedPFO  = nullptr;//
	std::vector<size_t> *m_EMPFNeutralPFO  = nullptr;//
	std::vector<double> *m_MatchedLCTopojetEta = nullptr;
	std::vector<double> *m_MatchedLCTopojetPhi = nullptr;
	std::vector<double> *m_MatchedLCTopojetPt  = nullptr;
	std::vector<double> *m_MatchedLCTopojetE   = nullptr;
	std::vector<double> *m_MatchedLCTopojetM   = nullptr;
	std::vector<double> *m_MatchedLCTopojetR   = nullptr;
	std::vector<size_t> *m_LCTopoChargedTrack  = nullptr;//
	std::vector<size_t> *m_LCTopoChargedPFO  = nullptr;//
	std::vector<size_t> *m_LCTopoNeutralPFO  = nullptr;//
	//matched Truth tau
	std::vector<double> *m_MatchedEMPFtauEta = nullptr;
	std::vector<double> *m_MatchedEMPFtauPhi = nullptr;
	std::vector<double> *m_MatchedEMPFtauPt  = nullptr;
	std::vector<double> *m_MatchedEMPFtauE   = nullptr;
	std::vector<double> *m_MatchedEMPFtauE_NPion = nullptr;
	std::vector<double> *m_MatchedEMPFtauM   = nullptr;
	std::vector<double> *m_MatchedEMPFtauR   = nullptr;
	std::vector<size_t> *m_EMPFChargedPion   = nullptr;//
	std::vector<size_t> *m_EMPFNeutralPion   = nullptr;//
	std::vector<double> *m_MatchedLCTopotauEta = nullptr;
	std::vector<double> *m_MatchedLCTopotauPhi = nullptr;
	std::vector<double> *m_MatchedLCTopotauPt  = nullptr;
	std::vector<double> *m_MatchedLCTopotauE   = nullptr;
	std::vector<double> *m_MatchedLCTopotauE_NPion = nullptr;
	std::vector<double> *m_MatchedLCTopotauM   = nullptr;
	std::vector<double> *m_MatchedLCTopotauR   = nullptr;
	std::vector<size_t> *m_LCTopoChargedPion = nullptr;//
	std::vector<size_t> *m_LCTopoNeutralPion = nullptr;//

private:

};

#endif
