#ifndef MyAnalysis_TauEle_H
#define MyAnalysis_TauEle_H

#include <xAODEgamma/ElectronContainer.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>

#include <xAODTau/TauJet.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODTau/TauDefs.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>

#include <xAODTruth/TruthEvent.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEventAuxContainer.h>

#include <xAODTruth/TruthPileupEvent.h>
#include <xAODTruth/TruthPileupEventContainer.h>
#include <xAODTruth/TruthPileupEventAuxContainer.h>

#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticleAuxContainer.h>

#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthVertexAuxContainer.h>

#include <xAODBase/IParticle.h>

#include <iostream>
#include <typeinfo>
#include <map>
#include <math.h>

class TauEle : public EL::AnaAlgorithm
{
public:
	
	TauEle (const std::string& name, ISvcLocator* pSvcLocator);

	// these are the functions inherited from Algorithm
	virtual StatusCode initialize () override;
	virtual StatusCode execute () override;
	virtual StatusCode finalize () override;
	

	~TauEle () override;

	//GRL
    //#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
    //#include <AsgTools/ToolHandle.h>
	//ToolHandle<IGoodRunsListSelectionTool> m_grl;
	// Global info
	unsigned int m_runNumber = 0; ///< Run number
	unsigned long long m_eventNumber = 0; ///< Event number
	
	int m_nMatched = 0;
	// Jet info
	std::vector<double> *m_EMPFjetEta = nullptr;
	std::vector<double> *m_EMPFjetPhi = nullptr;
	std::vector<double> *m_EMPFjetPt  = nullptr;
	std::vector<double> *m_EMPFjetE   = nullptr;
	std::vector<double> *m_EMPFjetM   = nullptr;
	std::vector<double> *m_LCTopojetEta = nullptr;
	std::vector<double> *m_LCTopojetPhi = nullptr;
	std::vector<double> *m_LCTopojetPt  = nullptr;
	std::vector<double> *m_LCTopojetE   = nullptr;
	std::vector<double> *m_LCTopojetM   = nullptr;
	//Truth tau
	std::vector<int> *m_tauNum = nullptr;
	std::vector<double> *m_tauEta = nullptr;
	std::vector<double> *m_tauPhi = nullptr;
	std::vector<double> *m_tauPt  = nullptr;
	std::vector<double> *m_tauE   = nullptr;
	std::vector<double> *m_tauE_NPion = nullptr;
	std::vector<double> *m_tauM   = nullptr;
	std::vector<size_t> *m_ChargedPion = nullptr;//
	std::vector<size_t> *m_NeutralPion = nullptr;//
	//taujet
	std::vector<double> *m_EleEta = nullptr;
	std::vector<double> *m_ElePhi = nullptr;
	std::vector<double> *m_ElePt  = nullptr;
	std::vector<double> *m_EleE   = nullptr;
	std::vector<double> *m_EleM   = nullptr;
	//matched taujet
	std::vector<double> *m_MatchedEleEta = nullptr;
	std::vector<double> *m_MatchedElePhi = nullptr;
	std::vector<double> *m_MatchedElePt  = nullptr;
	std::vector<double> *m_MatchedEleE   = nullptr;
	std::vector<double> *m_MatchedEleM   = nullptr;
	//matched Truth tau
	std::vector<double> *m_MatchedtauEta = nullptr;
	std::vector<double> *m_MatchedtauPhi = nullptr;
	std::vector<double> *m_MatchedtauPt  = nullptr;
	std::vector<double> *m_MatchedtauE   = nullptr;
	std::vector<double> *m_MatchedtauM   = nullptr;
	
    std::vector<double> *m_MatchedjetEta = nullptr;
	std::vector<double> *m_MatchedjetPhi = nullptr;
	std::vector<double> *m_MatchedjetPt  = nullptr;
	std::vector<double> *m_MatchedjetE   = nullptr;
	std::vector<double> *m_MatchedjetM   = nullptr;
    
    std::vector<double> *m_MatchedjettauEta = nullptr;
	std::vector<double> *m_MatchedjettauPhi = nullptr;
	std::vector<double> *m_MatchedjettauPt  = nullptr;
	std::vector<double> *m_MatchedjettauE   = nullptr;
	std::vector<double> *m_MatchedjettauM   = nullptr;

private:

};

#endif
