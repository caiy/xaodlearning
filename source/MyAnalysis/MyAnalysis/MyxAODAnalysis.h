#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

//below 2 are GRL tool
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TTree.h>
#include <xAODJet/JetContainer.h>

#include <vector>


class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
	
	MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

	// these are the functions inherited from Algorithm
	virtual StatusCode initialize () override;
	virtual StatusCode execute () override;
	virtual StatusCode finalize () override;

	~MyxAODAnalysis () override;

	//GRL
	// Global info
	unsigned int m_runNumber = 0; ///< Run number
	unsigned long long m_eventNumber = 0; ///< Event number
	
	// Jet info
	std::vector<float> *m_jetEta = nullptr;
	std::vector<float> *m_jetPhi = nullptr;
	std::vector<float> *m_jetPt = nullptr;
	std::vector<float> *m_jetE = nullptr;

private:

};

#endif
