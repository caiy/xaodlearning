#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
		action = 'store', type = 'string', default = 'submitDir',
		help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath = '/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/TauCP_AOD/mc16_13TeV.425200.Pythia8EvtGen_A14NNPDF23LO_Gammatautau_MassWeight.recon.AOD.e5468_s3126_r11153/'
ROOT.SH.ScanDir().filePattern( 'AOD.16538579._000001.pool.root.1' ).scan( sh, inputFilePath )
sh.printContent()


# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#can remove MaxEvents
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
#or:
#from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
#alg = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Add output stream for writing tree
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Add Tools
from AnaAlgorithm.DualUseConfig import addPrivateTool
# add the GRL tool to the algorithm
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )

# configure GRL tool properties
fullGRLFilePath = os.getenv ("ALRB_TutorialData") + "/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"
alg.grlTool.GoodRunsListVec = [ fullGRLFilePath ]
alg.grlTool.PassThrough = 1 # if true (default) will ignore result of GRL and will just pass all events

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

