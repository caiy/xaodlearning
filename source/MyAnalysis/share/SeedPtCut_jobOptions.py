
# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
#import optparse
#parser = optparse.OptionParser()
#parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
#		action = 'store', type = 'string', default = 'submitDir',
#		help = 'Submission directory for EventLoop' )
#parser.add_option( '-f', '--file-pattern', dest = 'file_pattern',
#		action = 'store', type = 'string', 
#		help = 'File Pattern, *:Any character' )
#parser.add_option( '-i', '--input-file', dest = 'input_file',
#		action = 'store', type = 'string', 
#		help = 'input samples' )
#( options, args ) = parser.parse_args()

#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

#testFile = os.getenv("ALRB_TutorialData") + '/r9315/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e5458_s3126_r9364_r9315/AOD.11182705._000001.pool.root.1'

#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [ "/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/GammaTT/EMPFlow/valid1.425200.Pythia8EvtGen_A14NNPDF23LO_Gammatautau_MassWeight.recon.AOD.e5468_e5984_s3583_r12103_r12119_r12159/AOD.22810429._000002.pool.root.1"] 
#jps.AthenaCommonFlags.FilesInput = [testFile] 

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:MyxAODAnalysis.outputs.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 


# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'TruthAna', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
#theApp.EvtMax = 500

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

