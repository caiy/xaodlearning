#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
		action = 'store', type = 'string', default = 'submitDir',
		help = 'Submission directory for EventLoop' )
parser.add_option( '-f', '--file-pattern', dest = 'file_pattern',
		action = 'store', type = 'string', 
		help = 'File Pattern, *:Any character' )
parser.add_option( '-i', '--input-dir', dest = 'input_dir',
		action = 'store', type = 'string', 
		help = 'folder of input samples' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
#ROOT.xAOD.Init().ignore()
assert ROOT.xAOD.Init().isSuccess()
ROOT.xAOD.TauJetContainer()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath = options.input_dir
ROOT.SH.ScanDir().filePattern( options.file_pattern ).scan( sh, inputFilePath )
sh.printContent()


# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#can remove MaxEvents
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'no-clobber')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'TruthAna', 'AnalysisAlg' )
#or:
#from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
#alg = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Add output stream for writing tree
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Add Tools
#from AnaAlgorithm.DualUseConfig import addPrivateTool
# add the GRL tool to the algorithm
#addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )

# configure GRL tool properties
#fullGRLFilePath = os.getenv ("ALRB_TutorialData") + "/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"
#alg.grlTool.GoodRunsListVec = [ fullGRLFilePath ]
#alg.grlTool.PassThrough = 1 # if true (default) will ignore result of GRL and will just pass all events

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

