from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm

def makeSequence (dataType) :
	algSeq = AlgSequence()
	return algSeq

# Set up the systematics loader/handler algorithm (algSeq) :
sysLoader = createAlgorithm( 'CP::SysListLoaderAlg', 'SysLoaderAlg' )
sysLoader.sigmaRecommended = 1
algSeq += sysLoader

# Include, and then set up the muon analysis algorithm sequence:
	# create the sequence of muon algorithms
from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
muonSequenceMedium = makeMuonAnalysisSequence( dataType, deepCopyOutput = True, shallowViewOutput = False, 
		workingPoint = 'Medium.Iso', postfix = 'medium' )
#a special post-configuration step (old fashion)
muonSequenceMedium.configure( inputName = 'Muons', 
		outputName = 'AnalysisMuonsMedium_%SYS%' )
# Add the sequence to the job:
algSeq += muonSequenceMedium


# Set up the muon calibration and smearing algorithm:
alg = createAlgorithm( 'CP::MuonCalibrationAndSmearingAlg', 
		'MuonCalibrationAndSmearingAlg' + postfix )
alg.preselection = "&&".join (selectionDecorNames)
addPrivateTool( alg, 'calibrationAndSmearingTool',
		'CP::MuonCalibrationPeriodTool' )
seq.append( alg, inputPropName = 'muons', outputPropName = 'muonsOut',
		affectingSystematics = '(^MUON_ID$)|(^MUON_MS$)|(^MUON_SAGITTA_.*)|(^MUON_SCALE$)',
		stageName = 'calibration' )

# Add an ntuple dumper algorithm:
treeMaker = createAlgorithm( 'CP::TreeMakerAlg', 'TreeMaker' )
treeMaker.TreeName = 'muons'
algSeq += treeMaker
ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMakerEventInfo' )
ntupleMaker.TreeName = 'muons'
ntupleMaker.Branches = [ 'EventInfo.runNumber     -> runNumber',
						 'EventInfo.eventNumber   -> eventNumber', ]
ntupleMaker.systematicsRegex = '(^$)'
algSeq += ntupleMaker
ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMakerMuons' )
ntupleMaker.TreeName = 'muons'
ntupleMaker.Branches = [ 'AnalysisMuonsMedium_NOSYS.eta -> mu_eta',
				         'AnalysisMuonsMedium_NOSYS.phi -> mu_phi',
						 'AnalysisMuonsMedium_%SYS%.pt  -> mu_%SYS%_pt', ]
ntupleMaker.systematicsRegex = '(^MUON_.*)'
algSeq += ntupleMaker
treeFiller = createAlgorithm( 'CP::TreeFillerAlg', 'TreeFiller' )
treeFiller.TreeName = 'muons'
algSeq += treeFiller

