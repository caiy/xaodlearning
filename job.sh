rm -f sub.sh.*
output_path="/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/TauCP_AOD/Ntuples/"
#input_path="/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/TauCP_AOD/mc16_13TeV.425200.Pythia8EvtGen_A14NNPDF23LO_Gammatautau_MassWeight.recon.AOD.e5468_s3126_r11153/"
begin_with="AOD.16538579._000"
end_with=".pool.root.1"

i=0
for (( one=0 ; one<=2 ; one++))
do
	for (( two=0 ; two<=9 ; two++))
	do
	
		for (( three=0 ; three<=9 ; three++))
		do
			echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" > sub.sh."$i"
			echo "alias setupATLAS='source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh' " >> sub.sh."$i"
			echo "setupATLAS" >> sub.sh."$i"
			echo "cd ../build " >> sub.sh."$i"
			echo "asetup --restore" >> sub.sh."$i"
			echo "source x86_64-*/setup.sh" >> sub.sh."$i"
			echo "cd ../subjob " >> sub.sh."$i"
			echo "TruthAna_0_eljob.py -s ${i} -f ${begin_with}${one}${two}${three}${end_with} " >> sub.sh."$i"
			chmod u+x sub.sh."$i"
			chmod +x sub.sh."$i"
			#nohup TruthAna_0_eljob.py -s="${output_path}${i}" -f="${begin_with}${one}${two}*${end_with}" >"${output_path}result${i}" &
			let i++
		done
	done
done

hep_sub -os SL7 sub.sh."%{ProcId}" -g atlas -n "$i"
#nohup TruthAna_0_eljob.py -s=/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/TauCP_AOD/Ntuples/0 -f=AOD.16538579._000166.pool.root.1  >result0 & 
#nohup TruthAna_0_eljob.py -s=/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/TauCP_AOD/Ntuples/1 -f=AOD.16538579._000166.pool.root.1  >result1 &
#nohup TruthAna_0_eljob.py -s=/publicfs/atlas/atlasnew/SUSY/users/caiyc/softTau/TauCP_AOD/Ntuples/2 -f=AOD.16538579._000166.pool.root.1  >result2 &
