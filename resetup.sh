#!/bin/bash
if [ $1 == "EL" ]
then
	echo Eventloop
	cd ./build
	asetup --restore
	source x86_64-*/setup.sh
elif [ $1 == "ATH" ]
then
	echo ATH
	cd ./buildath
	asetup --restore
	source x86_64-*/setup.sh
fi

#asetup 21.2,AnalysisBase,latest
#asetup 21.2.125,AthAnalysis
